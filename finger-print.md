# finger-print generation

This module is utilized to generate a signature for a web browser or even a crawler 

### different fingerprints

1. fingerprint based on ip address: 127.0.0.1 => 7f000001
2. fingerprint based on http headers: 

### the known methods for finger-printing

The main stream finger-printing method is known as browser tracking technology. In general, the technology consists of:

1. 17 browser's features defined in `AmIUnique paper`, including fonts, canvas rendering, audio context, virtual cpu cores;
2. atomic fingerprintable features;
3. composite fingerprintable features;


### the known projects for finger-printing



### user-agent database
https://developers.whatismybrowser.com/
how to interpret user-agent string?

### http headers fingerprint
User-Agent
Origin
Content-type
Accept-Encoding
Accept-Language
Referer


### all possible features for fingerprint
the User agent header
the Accept header
the Connection header
the Encoding header
the Language header
the list of plugins
the platform
the cookies preferences (allowed or not)
the Do Not Track preferences (yes, no or not communicated)
the timezone
the screen resolution and its color depth
the use of local storage
the use of session storage
a picture rendered with the HTML Canvas element
a picture rendered with WebGL
the presence of AdBlock
the list of fonts

### how to introduce entropy algorithms to the fingerprint and prove its validity



### some intuitions for static fingerprint based on request

1.user-agent is a very import part of the request
2.user-agent only could not guarantee high accuracy
3.



### session group policies for different type of crawler
1. crawler that receive cookie and execute js 
2. receive cookie but not execute js 
3. not receive cookie but execute js 
4. not receive cookie and not execute js 

ip fingerprint
static header fingerprint
cookie fingerprint
javascript fingerprint


### how to define static header fingerprint

header-key: header-value


crucial headers: 
user-agent + user-agent-value => hash_1   0.6
hash(header-key-1 + header-key-2 + header-key-3) => hash_2 0.3
hash('header-key' + 'header-value') => hash_group  0.1 for each one

url [20:]

[hash_1, hash_2, [hash_group]]




### fingerprint weight



### static javascript fingerprint 

<script src='http://172.16.10.1/fingerprint.js'></script>
<script>
var fp1 = new Fingerprint();
var fp2 = new Fingerprint({canvas: true});
var fp3 = new Fingerprint({ie_activex: true});
var fp4 = new Fingerprint({screen_resolution: true});
var fp_1 = fp1.get().toString('16');
var fp_2 = fp2.get().toString('16');
var fp_3 = fp3.get().toString('16');
var fp_4 = fp4.get().toString('16');
var fp = fp_1 + fp_2 + fp_3 + fp_4;
console.log(fp);
</script>



### database clean

blacklist


ip update:
update  black_data0913 set sip = '10.74.175.134'  where id=112;
update  black_data0913 set sip = '10.74.52.177'  where id=75 or id=112 or id=89;

