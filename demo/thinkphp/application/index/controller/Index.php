<?php
namespace app\index\controller;

class Index
{
    public function index()
    {
        return '<style type="text/css">*{ padding: 0; margin: 0; } .think_default_text{ padding: 4px 48px;} a{color:#2E5CD5;cursor: pointer;text-decoration: none} a:hover{text-decoration:underline; } body{ background: #fff; font-family: "Century Gothic","Microsoft yahei"; color: #333;font-size:18px} h1{ font-size: 100px; font-weight: normal; margin-bottom: 12px; } p{ line-height: 1.6em; font-size: 42px }</style><div style="padding: 24px 48px;"> <h1>:)</h1><p> ThinkPHP V5<br/><span style="font-size:30px">十年磨一剑 - 为API开发设计的高性能框架</span></p><span style="font-size:22px;">[ V5.0 版本由 <a href="http://www.qiniu.com" target="qiniu">七牛云</a> 独家赞助发布 ]</span></div><script type="text/javascript" src="https://tajs.qq.com/stats?sId=9347272" charset="UTF-8"></script><script type="text/javascript" src="https://e.topthink.com/Public/static/client.js"></script><think id="ad_bd568ce7058a1091"></think>';
    }

   public function show($id='not set'){

   		$page_number = input('?get.page')?input('get.page'):0;
   		

   		if ($id == 'not set'){
   			$res_file = "/res/res.json";
   			$id = 'tmp';
   		}else{
   			$res_file = "/tmp/upload/".$id."/".$id.".json";
   		}

   		if(!file_exists($res_file)){
   			return '<script>alert("not results!");window.location.href="/index/index/error";</script>';
   		}


   		
   		$json_result = json_decode(file_get_contents($res_file),true);
   		// var_dump($json_result);
   		$dynamic_sess_groups = $json_result['dynamic_sess_groups'];
   		$static_sess_groups = $json_result['static_sess_groups'];
		$all_sess_groups = array_merge($dynamic_sess_groups,$static_sess_groups);

		$res_sess_groups = array();
		$crawler_sess_count = 0;
		$malicious_sess_count = 0;
		$all_sess_count = 0;
		$all_request_count = 0;

		$page_size = 20;
   		$page_count = ceil($all_sess_count / $page_size);
   		$page_start = $page_size * $page_number;
   		$page_end = $page_start + $page_size - 1;
   		$res_sess_groups = array_slice($res_sess_groups,$page_start,20);

		foreach ($all_sess_groups as $sess_group) {

			# add rank
			if($sess_group['crawler']=='crawler'){
				$sess_group['rank'] = 2;
				$crawler_sess_count += 1;
			}elseif ($sess_group['crawler']=='malicious-crawler'){
				$sess_group['rank'] = 3;
				$malicious_sess_count += 1;
			}else{
				$sess_group['rank'] = 1;
			}

			# calc dynamic_fp
			if($sess_group['type']=='dynamic'){
				$sess_group['dynamic_fp'] = end($sess_group['fp']);
			}else{
				$sess_group['dynamic_fp'] = "None";
			}

			# calc static_fp
			$sess_group['static_fp'] = $sess_group['fp'][0].'-'.$sess_group['fp'][1].'-'.$sess_group['fp'][2];
			if(count($sess_group['static_fp'][3])>0){
				$sess_group['static_fp'] = $sess_group['static_fp'].'-'.implode('-',$sess_group['fp'][3]);
			}

			# calc requests_str
			$sess_group['requests_str'] = implode(', ',$sess_group['requests']);

			# calc ips_str
			$sess_group['ips_str'] = implode(', ',$sess_group['ips']);

			# calc id
			$all_sess_count += 1;
			$sess_group['id'] = md5($sess_group['static_fp'] . $sess_group['dynamic_fp']);
			array_push($res_sess_groups,$sess_group);
			$all_request_count += count($sess_group['requests']);
		}


		



   		$sum_info_array = [$all_request_count, $all_sess_count, $crawler_sess_count, $malicious_sess_count];

   		return view('show',['info'=>$sum_info_array,'sess_list'=>$res_sess_groups,'page_number'=>$page_number, 'page_count' => $page_count]);

	}



	public function error(){

			return '500 error';
	}

	



}
