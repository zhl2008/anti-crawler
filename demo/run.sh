#!/bin/sh

cp /var/www/html/000-default.conf /etc/apache2/sites-enabled/000-default.conf
service apache2 start
service mysql start
rm -rf /tmp/upload
mkdir /tmp/upload
chown -R www-data:www-data /tmp/upload
/bin/bash
