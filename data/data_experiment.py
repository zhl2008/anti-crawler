#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import json
import MySQLdb
import random
import hashlib
import time
import sys
import redis

reload(sys)  
sys.setdefaultencoding('utf8')

'''
snippet for Chinese character support
'''

'''
structure data example: 
{"request_time": 1561956984.3283665, "src_ip": "172.16.10.1", "dst_ip": "172.16.10.1", "path": "/plus/qrcode.php?action=get_qrcode&type=index&id=0", "content": "", "cookie": {}, "headers": {"Host": "172.16.10.1", "Connection": "keep-alive", "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) HeadlessChrome/70.0.3509.0 Safari/537.36", "Accept": "image/webp,image/apng,image/*,*/*;q=0.8", "Referer": "http://172.16.10.1:8001/plus/qrcode.php?id=0&type=index", "Accept-Encoding": "gzip, deflate"}, "response_code": 200, "response_len": 420}
'''



def load_data_from_db(start,limit):
	db = MySQLdb.connect("127.0.0.1", "root", "root", "data", charset='utf8' )
	cursor = db.cursor()
	sql = 'select * from black_data0913 where packet_data!="" limit %d,%d;' %(start,limit)
	
	bad_ids = load_bad_id()
	# print bad_ids

	try:
		cursor.execute(sql)
		results = cursor.fetchall()
		for row in results:
			tmp = {}
			_id = row[0]

			# prevent bad_ids

			if str(_id) in bad_ids:
				continue

			tmp['_id'] = _id
			tmp['src_ip'] = row[2]
			tmp['dst_ip'] = row[3]
			tmp['path'] = row[4]
			tmp['User-Agent'] = row[12]
			tmp['data'] = row[13]
			tmp['response_code'] = 200
			tmp['response_len'] = 420
			tmp['headers'] = {}
			tmp['cookie'] = ''

			# parse the headers
			headers = row[14].split('\r\n\r\n')[0].split('\r\n')[1:]

			for header in headers:

				# may be parse error here, just ignore it
				try:
					header_key,header_value = header.split(': ')

					# # special handler for cookie
					# if header_key.lower() == 'cookie':
					# 	tmp['cookie'] = header_value
					# 	continue
					# ignore user-agent in headers
					if header_key.lower() == 'user-agent':
						continue

					tmp['headers'][header_key] = header_value
				except Exception as e:
					pass

			# # user-agent filter
			# if not tmp['User-Agent']:
			# 	continue


			# print tmp['headers']
			# print tmp
			request_datas[_id] = tmp

	except Exception as e:
		# print row[14]
		print e


def load_data_from_db_2(start,limit):
	db = MySQLdb.connect("127.0.0.1", "root", "root", "data", charset='utf8' )
	cursor = db.cursor()
	sql = 'select id,sip,dip,uri,agent,host,origin,referer,xff,content_type,accept_language,cookie,`@timestamp` from 166_111_5_35   order by `@timestamp` limit %d,%d;' %(start,limit)
	
	# bad_ids = load_bad_id()
	# print bad_ids

	try:
		cursor.execute(sql)
		results = cursor.fetchall()
		for row in results:
			tmp = {}
			_id = row[0]

			# prevent bad_ids
			# if str(_id) in bad_ids:
			# 	continue

			tmp['_id'] = _id
			tmp['src_ip'] = row[1]
			tmp['dst_ip'] = row[2]
			tmp['path'] = row[3]
			tmp['User-Agent'] = row[4]
			tmp['timestamp'] = row[12]
			tmp['response_code'] = 200
			tmp['response_len'] = 420

			tmp['timestamp'] = tmp['timestamp'].split('.')[0]
			tmp['timestamp'] = time.mktime(time.strptime(tmp['timestamp'],"%Y-%m-%dT%H:%M:%S"))
			
			

			tmp['headers'] = {"Host":row[5],"Origin":row[6],"Referer":row[7],"X-Forwarded-For":row[8],"Content-Type":row[9],"Accept-Language":row[10]}
			tmp['cookie'] = row[11]

			tmp['headers'] = dict( (k,v) for k,v in tmp['headers'].iteritems() if v )

			request_datas[_id] = tmp


	except Exception as e:
		# print row[14]
		print e


def load_data_from_db_3(start,limit):
	client =redis.Redis(host='127.0.0.1',port=6379,db=0)

	try:
		results = client.hgetall('my_request')
		for _key in results:
			row = json.loads(results[_key])
			tmp = {}
			_id = _key
			# prevent bad_ids
			# if str(_id) in bad_ids:
			# 	continue


			tmp['_id'] = _id
			tmp['src_ip'] = row['src_ip']
			tmp['dst_ip'] = row['dst_ip']
			tmp['path'] = row['path']
			tmp['User-Agent'] = row['headers']['User-Agent']
			tmp['timestamp'] = row['request_time']
			tmp['response_code'] = row['response_code']
			tmp['response_len'] = row['response_len']
			tmp['cookie'] = ''
			tmp['headers'] = row['headers']
			tmp['js_fp'] = row['js_fp']
			
			# remove the empty data
			if tmp['js_fp'] == 'unknown':
				continue
				
			request_datas[_id] = tmp

			# print request_datas[_id]
	except Exception as e:
		# print row[14]
		print e


def session_group_base_on_ip():
	'''
	group session by source ip address
	'''
	session_groups = {}
	ip_session_map = {}

	sess_id_count = 1
	for _id in request_datas:
		src_ip = request_datas[_id]['src_ip']
		dst_ip = request_datas[_id]['dst_ip']

		# src_ip already in session
		if (src_ip + '_' + dst_ip) in ip_session_map:
			session_id = ip_session_map[src_ip + '_' + dst_ip]
			session_groups[session_id].append(_id)

		# src_ip already in session
		else:
			session_id = sess_id_count
			ip_session_map[src_ip + '_' + dst_ip] = session_id
			session_groups[session_id] = [_id]
			sess_id_count += 1


	return session_groups,ip_session_map


def session_group_base_on_fp():
	'''
	group session by source ip address, fp_session_map:  session_id => fp_array
	'''

	global score_thresold

	session_groups = {}
	fp_session_map = {}
	
	sess_id_count = 1
	for _id in request_datas:
		fp = calc_fp(request_datas[_id])

		# # fp already in session and the sequence span should less than 1000 
		# if fp in fp_session_map:
		# 	session_id = fp_session_map[fp]
		# 	session_groups[session_id].append(_id)

		
		# else:
		# 	session_id = sess_id_count
		# 	fp_session_map[fp] = session_id
		# 	session_groups[session_id] = [_id]
		# 	sess_id_count += 1
		max_score = 0
		max_score_id = -1
		

		for session_id in fp_session_map:
			session_fp = fp_session_map[session_id]
			tmp_score = calc_fp_similarity(fp,session_fp)
			# update max score and id
			if tmp_score > max_score:
				max_score_id = session_id
				max_score = tmp_score

		# find match
		if max_score > score_thresold:
			session_groups[max_score_id].append(_id)

		# not match then create new session
		else:
			session_id = sess_id_count
			fp_session_map[session_id] = fp
			session_groups[session_id] = [_id]
			sess_id_count += 1


	return session_groups,fp_session_map


def calc_fp(request_data):

	fp_group_penalty = ['Connection','Accept','Content-Type']

	fp_1 = hashlib.md5(request_data['User-Agent']).hexdigest() if (request_data.has_key('User-Agent') and  request_data['User-Agent']) else '0'*32
	fp_2 = hashlib.md5(''.join(request_data['headers'].keys())).hexdigest()
	fp_3 = hashlib.md5(request_data['path'][:20]).hexdigest() if request_data['path'] else '0'*32
	# print request_data['headers'].keys()

	
	fp_penalty = [ hashlib.md5(request_data['headers'][x]).hexdigest() for x in fp_group_penalty if request_data['headers'].has_key(x)]
	fp_group = [ hashlib.md5(request_data['headers'][x]).hexdigest() for x in request_data['headers'].keys() if (len(x) > 0 and x not in fp_group_penalty)]

	fp_js = request_data['js_fp']
	# print [fp_1, fp_2, fp_3, fp_group]
	return [fp_1, fp_2, fp_3, fp_group,fp_penalty,fp_js]


def calc_fp_similarity(fp_1,fp_2):
	'''
	This function is utilized to calc the similarity between 2 fingerprints

	'''

	global weight

	score = 0
	

	if fp_1[0] == fp_2[0] and fp_1[0] != '00000000000000000000000000000000':
		score += weight[0]

	if fp_1[1] == fp_2[1] and fp_1[1] != '00000000000000000000000000000000':
		score += weight[1]

	if fp_1[2] == fp_2[2] and fp_1[2] != '00000000000000000000000000000000':
		score += weight[2]

	# fp_group for header fp
	for i in range(len(fp_1[3])):
		if fp_1[3][i] in fp_2[3]:
			score += weight[3]

	# for fp_penalty
	for i in range(len(fp_1[4])):
		if fp_1[4][i] in fp_2[4]:
			score += weight[4]

	# with dynamic fingerprint
	if len(fp_1) == 6:
		if fp_1[5] == fp_2[5]:
			score += weight[5]
	
	return score/float( max(len(fp_1[3])+len(fp_1[4]) , len(fp_2[3])+len(fp_1[4]))   + 2 )



def load_bad_id():
	bad_id_file = './bad_id'
	bad_ids = [x.strip() for x in open(bad_id_file).readlines() if x]
	return bad_ids


def db_to_json():
	global request_datas
	load_data_from_db_2(0,100000)
	open('./db.json','w').write(json.dumps(request_datas))

def black_to_json():
	global request_datas
	load_data_from_db(0,100000)
	open('./db_black.json','w').write(json.dumps(request_datas))

def redis_to_json():
	global request_datas
	load_data_from_db_3(0,100000)
	open('./db_redis.json','w').write(json.dumps(request_datas))


def load_data_from_json(start,length,json_file='./db.json'):
	global request_datas,my_data
	# load from the file if the my_data is not in the memory
	if len(my_data) == 0:
		my_data = json.loads(open(json_file).read())
		if 'timestamp' in my_data.values()[0].keys():
			my_data = sorted(my_data.items(), key = lambda x:x[1]['timestamp'])
		else:
			my_data = sorted(my_data.items(), key = lambda x:x[1]['_id'])
	
	# print [x[1]['timestamp'] for x in my_data[start:start+length]]
	request_datas = dict(my_data[start:start+length])


def check_session_group(session_groups,old_session_groups,_print=0):
	final_score = 0
	for session_group_id in session_groups:
		session_group = session_groups[session_group_id]

		# summary the src_ip in each session
		src_ip_tables = {}
		for _id in session_group:
			src_ip = request_datas[_id]['src_ip']
			if src_ip in src_ip_tables:
				src_ip_tables[src_ip] += 1
			else:
				src_ip_tables[src_ip] =1

		# print src_ip_tables
		accurate_ratio = float(max(src_ip_tables.values()))/sum(src_ip_tables.values())
		final_score += max(src_ip_tables.values()) 
		if _print:
			print 'accurate ratio for session: %d is %f with count:%d ip_tables:%s' %(session_group_id,accurate_ratio,len(session_groups[session_group_id]), str(src_ip_tables))
			print 'session_groups: %s' %(str(session_groups[session_group_id]))

	extra_group_penalty = len(session_groups) - len(old_session_groups)
	if _print:
		print 'extra_group_penalty: %d' %extra_group_penalty
		print 'total request_data: %d' %(len(request_datas))
		print 'final_score: %d' %(final_score  - extra_group_penalty)
	# print 'ratio_socre: %f'%(float(final_score  - extra_group_penalty)/len(request_datas))
	return (final_score  - extra_group_penalty)



def run():

	global request_datas, score, my_data

	count = 0
	for i in range(0,4):

		request_datas = {}

		# load from db
		# load_data_from_db(1000*i,1000)

		# load from json
		# load_data_from_json(1000*i,1000,'./db.json')
		load_data_from_json(1000*i,1000,'./db_redis.json')
		# load_data_from_json(1000*i,1000,'./db_black.json')


		# tmp = [x['timestamp'] for x in request_datas.values()]
		# print sorted(tmp)
		count += len(request_datas)

		session_groups,fp_session_map =  session_group_base_on_ip()
		check_session_group(session_groups,session_groups,0)
		old_session_groups = session_groups
		# print '\n'
		# print '#' * 50
		# print '\n'
		session_groups,fp_session_map =  session_group_base_on_fp()
		tmp_score = check_session_group(session_groups,old_session_groups,0)
		score += tmp_score
		# print calc_fp(request_datas[38912])
		# print calc_fp(request_datas[38899])
		# print calc_fp_similarity(calc_fp(request_datas[87147016]),calc_fp(request_datas[87130118]))

	print score,count


if __name__ == '__main__':

	
	score = 0
	my_data = {}
	request_datas = {}

	# three types of dataset
	# db_to_json()
	# black_to_json()
	# redis_to_json()
	# exit()

	if len(sys.argv) > 1:
		args =  json.loads(sys.argv[1])
		weight = args[:6]
		score_thresold = args[6]
	else:
		weight = [0.6, 0.1, 0.2,0.1,0.1,0]
		score_thresold = 0.22


	run()
	

	


	# request_datas = {}
	# load_data_from_db(0,1000)
	# session_groups,fp_session_map =  session_group_base_on_ip()
	# check_session_group(session_groups,session_groups)
	# old_session_groups = session_groups
	# print '\n'
	# print '#' * 50
	# print '\n'
	# session_groups,fp_session_map =  session_group_base_on_fp()
	# tmp_score = check_session_group(session_groups,old_session_groups)


