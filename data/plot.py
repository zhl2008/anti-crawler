#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
reload(sys)  
sys.setdefaultencoding('utf8')
import matplotlib.pyplot as plt
import numpy as np
import matplotlib
import json
import MySQLdb
import random
import hashlib
import urlparse
import time

# matplotlib.rcParams['font.sans-serif']=['SimHei']  
matplotlib.rcParams['axes.unicode_minus']=False   



def load_data_from_db_2(start,limit):
	db = MySQLdb.connect("127.0.0.1", "root", "root", "data", charset='utf8' )
	cursor = db.cursor()
	sql = 'select id,sip,dip,uri,agent,host,origin,referer,xff,content_type,accept_language,cookie,`@timestamp`,method from 166_111_5_35 where length(agent)>0  order by `@timestamp` limit %d,%d;' %(start,limit)
	
	# bad_ids = load_bad_id()
	# print bad_ids

	try:
		cursor.execute(sql)
		results = cursor.fetchall()
		for row in results:
			tmp = {}
			_id = row[0]

			# prevent bad_ids
			# if str(_id) in bad_ids:
			# 	continue

			tmp['_id'] = _id
			tmp['src_ip'] = row[1]
			tmp['dst_ip'] = row[2]
			tmp['path'] = row[3]
			tmp['User-Agent'] = row[4]
			tmp['response_code'] = 200
			tmp['response_len'] = 420
			

			tmp['headers'] = {"Host":row[5],"Origin":row[6],"Referer":row[7],"X-Forwarded-For":row[8],"Content-Type":row[9],"Accept-Language":row[10]}
			tmp['cookie'] = row[11]
			tmp['timestamp'] = row[12]
			tmp['method'] = row[13]

	
			tmp['timestamp'] = tmp['timestamp'].split('.')[0]
			tmp['timestamp'] = time.mktime(time.strptime(tmp['timestamp'],"%Y-%m-%dT%H:%M:%S"))

			tmp['headers'] = dict( (k,v) for k,v in tmp['headers'].iteritems() if v )

			request_datas[_id] = tmp


	except Exception as e:
		# print row[14]
		print e


def session_group_base_on_ip():
	'''
	group session by source ip address
	'''
	session_groups = {}
	ip_session_map = {}

	sess_id_count = 1
	for _id in request_datas:
		src_ip = request_datas[_id]['src_ip']
		dst_ip = request_datas[_id]['dst_ip']

		# src_ip already in session
		if (src_ip + '_' + dst_ip) in ip_session_map:
			session_id = ip_session_map[src_ip + '_' + dst_ip]
			session_groups[session_id].append(_id)

		# src_ip already in session
		else:
			session_id = sess_id_count
			ip_session_map[src_ip + '_' + dst_ip] = session_id
			session_groups[session_id] = [_id]
			sess_id_count += 1

	return session_groups,ip_session_map


def find_robots_by_ua():

	crawler_strings = ['baiduspider','tsinghuaspider','yisouspider','360spider','sogou web spider','bytespider','magibot','coccocbot','semrushbot','piplbot','bingbot','googlebot','cliqzbot','exabot','ahrefsbot','yak','feeddemon', 'indy library', 'alexa toolbar', 'asktbfxtv', 'ahrefsbot', 'crawldaddy', 'coolpadwebkit', 'java', 'feedly', 'universalfeedparser', 'apachebench', 'microsoft url control', 'swiftbot', 'zmeu', 'obot', 'jaunty', 'python-urllib', 'lightdeckreports bot', 'yyspider', 'digext', 'httpclient', 'heritrix', 'easouspider', 'ezooms', 'headless', 'curl', 'wget','bot','spider','cfnetwork','python','crawler','go-http-client','ruby']

	crawler_group = {}
	crawler_group['count'] = 0
	for _id in request_datas:

		agent = request_datas[_id]['User-Agent'].lower()


		for crawler_string in crawler_strings:
			if crawler_string in agent:
				if crawler_group.has_key(crawler_string):
					crawler_group[crawler_string].append(agent)
				else:
					crawler_group[crawler_string] = [agent]
				crawler_group['count'] += 1
				break

	return crawler_group

def find_robots_by_refer():
	count = 0
	for _id in request_datas:
		if request_datas[_id]['headers'].has_key('Host') and request_datas[_id]['headers'].has_key('Referer'):
			host = request_datas[_id]['headers']['Host']
			refer = request_datas[_id]['headers']['Referer']
			if host and refer and host not in refer:
				count += 1
				# print host,refer,request_datas[_id]['User-Agent']
	print count
	


def find_robots_by_method():
	count = 0
	for _id in request_datas:
		if request_datas[_id]['method'] not in ['GET','POST']:
			print request_datas[_id]['method']
			count += 1
	print count

def find_robots_by_uri():
	count = 0
	for _id in request_datas:
		if 'robots.txt' in request_datas[_id]['path']:
			print request_datas[_id]['path']
			count += 1
	print count



def find_robots_by_a():
	count = 0
	for _id in request_datas:
		if 'robots.txt' in request_datas[_id]['path']:
			print request_datas[_id]['path']
			count += 1
	print count


def find_robots_by_malicious():
	count = 0
	for _id in request_datas:

		count += 1
	print count


def most_visited_url():

	url_group = {}
	for _id in request_datas:
		path = request_datas[_id]['path']
		# print path

		real_path = urlparse.urlparse(path).path
		if url_group.has_key(real_path):
			url_group[real_path] += 1
		else:
			url_group[real_path] = 1

	return url_group


def plot_hist(data):
	"""
	绘制直方图
	data:必选参数，绘图数据
	bins:直方图的长条形数目，可选项，默认为10
	normed:是否将得到的直方图向量归一化，可选项，默认为0，代表不归一化，显示频数。normed=1，表示归一化，显示频率。
	facecolor:长条形的颜色
	edgecolor:长条形边框的颜色
	alpha:透明度
	"""
	plt.hist(data, bins=40, normed=0, facecolor="blue", edgecolor="black", alpha=0.7)
	# 显示横轴标签
	plt.xlabel("session length")
	# 显示纵轴标签
	plt.ylabel("frequency")
	# 显示图标题
	plt.title("session length diagram(10-100)")
	plt.show()


def get_session_time(_ids):

	time_stamp_array = []
	for _id in _ids:
		timestamp = request_datas[_id]['timestamp']
		time_stamp_array.append(timestamp)

	return sorted(time_stamp_array)


def plot_pie(data):
	label_list = ["normal-request", "crawler"]    # 各部分标签
	size = data    # 各部分大小
	color = ["red", "green", ]     # 各部分颜色
	explode = [0 for x in range(len(size))]   # 各部分突出值
	"""
	绘制饼图
	explode：设置各部分突出
	label:设置各部分标签
	labeldistance:设置标签文本距圆心位置，1.1表示1.1倍半径
	autopct：设置圆里面文本
	shadow：设置是否有阴影
	startangle：起始角度，默认从0开始逆时针转
	pctdistance：设置圆内文本距圆心距离
	返回值
	l_text：圆内部文本，matplotlib.text.Text object
	p_text：圆外部文本
	"""
	patches, l_text, p_text = plt.pie(size, explode=explode, colors=color, labels=label_list, labeldistance=1.1, autopct="%1.1f%%", shadow=False, startangle=90, pctdistance=0.6)
	plt.axis("equal")    # 设置横轴和纵轴大小相等，这样饼才是圆的
	plt.title("traditional crawler detection/ (tsinghua univeristy)")
	plt.legend()
	plt.show()


def plot_pie_2(data):
	label_list = data.keys()    # 各部分标签
	size = [float(x)/ sum(data.values()) for x in data.values()]   # 各部分大小
	print label_list
	print size
	color = ["red", "green", ]     # 各部分颜色
	explode = [0 for x in range(len(size))]   # 各部分突出值
	"""
	绘制饼图
	explode：设置各部分突出
	label:设置各部分标签
	labeldistance:设置标签文本距圆心位置，1.1表示1.1倍半径
	autopct：设置圆里面文本
	shadow：设置是否有阴影
	startangle：起始角度，默认从0开始逆时针转
	pctdistance：设置圆内文本距圆心距离
	返回值
	l_text：圆内部文本，matplotlib.text.Text object
	p_text：圆外部文本
	"""
	patches, l_text, p_text = plt.pie(size, explode=explode, labels=label_list, labeldistance=1.1, autopct="%1.1f%%", shadow=False, startangle=90, pctdistance=0.6)
	plt.axis("equal")    # 设置横轴和纵轴大小相等，这样饼才是圆的
	plt.title("crawler types/ (tsinghua univeristy)")
	plt.legend()
	plt.show()


def label_crawler_session_by_ua(_ids):
	crawler_strings = ['baiduspider','tsinghuaspider','yisouspider','360spider','sogou web spider','bytespider','magibot','coccocbot','semrushbot','piplbot','bingbot','googlebot','cliqzbot','exabot','ahrefsbot','yak','feeddemon', 'indy library', 'alexa toolbar', 'asktbfxtv', 'ahrefsbot', 'crawldaddy', 'coolpadwebkit', 'java', 'feedly', 'universalfeedparser', 'apachebench', 'microsoft url control', 'swiftbot', 'zmeu', 'obot', 'jaunty', 'python-urllib', 'lightdeckreports bot', 'yyspider', 'digext', 'httpclient', 'heritrix', 'easouspider', 'ezooms', 'headless', 'curl', 'wget','bot','spider','cfnetwork','python','crawler','go-http-client','ruby']

	for _id in _ids:
		agent = request_datas[_id]['User-Agent'].lower()
		for crawler_string in crawler_strings:
			if crawler_string in agent:
				return True

	return False



def plot_normal(data):


	x1=[x for x in range(len(data))]
	y1=[data[x] for x in range(len(data))]
	print y1
	l1=plt.plot(x1,y1,'r--',label='type1')
	plt.plot(x1,y1,'ro-')
	plt.title('The Lasers in Three Conditions')
	plt.xlabel('row')
	plt.ylabel('column')
	plt.legend()
	plt.show()

if __name__ == '__main__':


	# session length diagram
	# request_datas = {}
	# load_data_from_db_2(0,100000)
	# session_groups,fp_session_map =  session_group_base_on_ip()
	# session_len = [len(x) for x in session_groups.values() if len(x)>10 and len(x)<100] 
	# session_len = np.array(session_len)
	# session_len = np.reshape(session_len,(len(session_len),-1))
	# print session_len
	# plot_hist(session_len)

	# tsinghua university dataset (normal detection)
	# plot_pie([float(100000-24323)/100000,float(24323)/100000])

	# spider type
	# spider_data = {"piplbot": 572,"semrushbot": 4586,"googlebot": 1159,"bytespider": 1505,"360spider": 1170,"yisouspider": 2005,"cfnetwork": 3012,"tsinghuaspider": 5762,"sogou web spider": 107,"baiduspider": 3309,"other": 1136}

	# plot_pie_2(spider_data)

	request_datas = {}
	load_data_from_db_2(0,100000)
	# res = find_robots_by_ua()
	# print res['count']
	# del res['count']
	# for r in res:
	# 	print r,len(res[r])

	# print res['spider']

	find_robots_by_uri()
	# most-visited url
	# request_datas = {}
	# load_data_from_db_2(0,100000)
	# print most_visited_url()


	# request_datas = {}
	# load_data_from_db_2(0,100000)
	# session_groups,fp_session_map =  session_group_base_on_ip()

	# session_groups = dict([(x,session_groups[x]) for x in session_groups if len(session_groups[x])>10 and len(session_groups[x])<100])
	
	# print session_groups.keys()
	
	# crawler_session_groups = [x for x in session_groups if label_crawler_session_by_ua(session_groups[x])]

	# print crawler_session_groups

	# print plot_normal(get_session_time(session_groups[8159])[:])






