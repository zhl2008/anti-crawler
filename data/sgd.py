#!/usr/bin/env python2
import os
import json
import random 

def f(x):
	res = os.popen('python2  data_experiment.py "' + json.dumps(x) +'"').read()
	print x
	print res
	return int(res.strip())

def error(x):
	return 1-(float(f(x))/(50000))

def my_gradient_descent(x,lr):

	delta = 0.01
	tmp = error(x)
	derivative_x = [((error([x[j] + delta if j==i else x[j] for j in range(len(x)) ]) - tmp) / delta) for i in range(len(x))]
	print derivative_x
	tmp = [ (x[i] - random.randint(1,9) * 0.002) if (derivative_x[i]==0 or derivative_x[i]>5 ) else (x[i] - lr * derivative_x[i]) for i in range(len(x)) ]
	return tmp



x = [0.6, 0.1, 0.2,0.1,0.1,0.22]

for i in range(1000):
	x = my_gradient_descent(x,0.02)
	# make sure that every element of x is larger than 0
	x = [x[i] if x[i]>0 else 0 for i in range(len(x))] 
	res = 'x = {:6f},{:6f},{:6f},{:6f},{:6f},{:6f}, error(x) = {:6f}'.format(x[0],x[1],x[2],x[3],x[4],x[5],error(x))
	open('res2.txt','a').write(res+"\n")
	print res


# my_gradient_descent(x,1)
# gradient_descent(x)



