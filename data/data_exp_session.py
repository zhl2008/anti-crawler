#!/usr/bin/env python2

import sys
import json
import MySQLdb
import random
import hashlib
import time
import sys
import numpy as np
from  urlparse import urlparse

reload(sys)  
sys.setdefaultencoding('utf8')


def load_spider_data_from_db(start,limit):
	db = MySQLdb.connect("127.0.0.1", "root", "root", "data", charset='utf8' )
	cursor = db.cursor()
	sql = 'select id,sip,dip,uri,status,`@timestamp`,data from 166_111_5_35 where agent like "%%baiduspider%%" limit %d,%d;' %(start,limit)
	try:
		cursor.execute(sql)
		results = cursor.fetchall()
		for row in results:
			tmp = {}
			_id = row[0]

			tmp['_id'] = _id
			tmp['src_ip'] = row[1]
			tmp['dst_ip'] = row[2]
			tmp['path'] = row[3]
			tmp['response_code'] = row[4]
			tmp['response_len'] = len(row[6])
			tmp['timestamp'] = row[5]
			tmp['timestamp'] = tmp['timestamp'].split('.')[0]
			tmp['timestamp'] = time.mktime(time.strptime(tmp['timestamp'],"%Y-%m-%dT%H:%M:%S"))
			request_datas[_id] = tmp

	except Exception as e:
		# print row[14]
		print e

def calc_session_features(data):

	# 4xx/5xx ratio
	_4xx_count = 0
	_5xx_count = 0
	for _id in data:
		d = data[_id]
		if str(d['response_code']).startswith('4'):
			_4xx_count += 1
		elif str(d['response_code']).startswith('5'):
			_5xx_count += 1

	_4xx_ratio = float(_4xx_count)/len(data)
	_5xx_ratio = float(_5xx_count)/len(data)

	print _4xx_ratio,_5xx_ratio,_4xx_count,_5xx_count,len(data)


	# page ratio
	not_page_postfix = ['.png','.jpg','.gif','.ico','.css','.js','.tiff','.mp3','.avi','.docx','.xml','.rar','.doc','.txt']
	page_count = 0
	for _id in data:
		d = data[_id]
		path = d['path']
		real_path = urlparse(path).path
		flag = 0
		for n in not_page_postfix:
			if n in real_path.lower():
				flag = 1
				break
		if not flag:
			# if '.' in real_path and '.html' not in real_path:
			# 	print real_path
			page_count += 1
	page_ratio = float(page_count)/len(data)

	print page_ratio,page_count,len(data)


	# request time standard deviation
	time_seq = []
	for _id in data:
		d = data[_id]
		timestamp = d['timestamp']
		time_seq.append(timestamp)
	time_seq = sorted(time_seq)

	max_freq = 0
	for i in range(len(time_seq)):
		time_pivot = time_seq[i]
		j = 1
		while j+i+1 < len(time_seq) and time_seq[j+i] <= time_pivot + 60:
			j += 1
		if j > max_freq:
			max_freq = j 

	time_span = [x -time_seq[0] for x in time_seq]
	# print time_span
	print max_freq,np.std(time_span)





if __name__ == '__main__':
	request_datas = {}
	load_spider_data_from_db(0,100)
	calc_session_features(request_datas)

