#include <QWebView>
#include <QtWidgets>
#include <stdio.h>
#include <QPrinter>
#include <stdint.h>
#include <stddef.h>
#include <string.h>

extern "C" int LLVMFuzzerTestOneInput(const uint8_t *Data, size_t Size) {
    //QString innerHtml = (char *) Data;
    int argc=0;char * argv[1];argv[0] = "";
    if(Size<=0){
        return 0;
    }
    char * tmp = (char *) Data;
    QString innerHtml = tmp;
    QApplication app(argc,argv);
    QString saveLocation = "/var/www/html/out1.png";
    QPrinter printer(QPrinter::HighResolution);
    printer.setOutputFileName(saveLocation);
    QWebView *view = new QWebView();
    view->setHtml(innerHtml);
    view->print(&printer);
    return 0;
}
