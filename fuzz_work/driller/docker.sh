#!/bin/sh

echo 1 | sudo tee /proc/sys/kernel/sched_child_runs_first
echo core | sudo tee /proc/sys/kernel/core_pattern

docker run --privileged -p 81:80 -p  2222:22 -v `pwd`:/root/run/ -ti introspelliam/qwb-ai:16.04  /bin/bash
