#!/bin/sh

# perform symbolic-assisted fuzzing with 4 AFL cores and 2 symbolic tracing (drilling) cores.
shellphuzz -i -c 4 -d 2 /path/to/binary
