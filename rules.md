# anti-crawler rules collection

session groups rules:

finger-print components:
1. ip_addr 4 bytes
2. user-header information: 4 bytes
3. JS fingerprint 4 bytes


stream analysis:
1. resort to part of the original data (may be 100,000 requests are enough to be analyzed)

2. observe some of its features, then establish the rules and try to adjust its threshold

3. try to use the hidden markov chain to train the model



### static rules

UA rules:

FeedDemon
Indy Library
Alexa Toolbar
AskTbFXTV
AhrefsBot
CrawlDaddy
CoolpadWebkit
Java
Feedly
UniversalFeedParser
ApacheBench
Microsoft URL Control
Swiftbot
ZmEu
oBot
jaunty
Python-urllib
lightDeckReports Bot
YYSpider
DigExt
HttpClient
MJ12bot
heritrix
EasouSpider
Ezooms
headless
curl
wget


the crawler type in dataset 1:
total: 24323

piplbot: 572
semrushbot: 4586
googlebot: 1159
bytespider: 1505
360spider: 1170
yisouspider: 2005
cfnetwork: 3012
tsinghuaspider: 5762
sogou web spider: 107
baiduspider: 3309
other: 1136

572 + 4586 + 1159 + 1505 + 1170 + 2005 + 3012 + 5762 + 107 + 3309 = 23187

crawler based on programming language(the default ua)
python: 27
curl: 90
java: 3
go-http-client: 8
wget: 1
ruby: 1



for that database, what kind of features u want to extract?

1. the proportion of the normal request and crawler request (pie graph)
2. session length distribution (hist graph)
3. crawler type distribution (barh graph)
4. most visited url (barh graph)
5. the timespan between each request in a session (?)


### session rules


the proportion of the 404 response
Percentage of 2xx responses
Percentage of 3xx responses
the proportion of the static request and the image request
Percentage of page requests

avg-time
min-time
max-time



session cluster: in our dataset, the crawler session could not be easily detected via the avg-time and the features of the dataset. 

However, in some scenarios, when the crawler are targeting at your data specifically, (1 to 1 model),you will receive a sequence of requests that conforms the regularity of the stream.










