"""
This example shows two ways to redirect flows to another server.
"""
from mitmproxy import http
from mitmproxy import ctx
import time
import json
import config
from function import *
from bs4 import BeautifulSoup
from urllib.parse import urlparse,parse_qsl
from payload import *
import hashlib
import random
import base64




class my_request():
	'''
	This class define the basic data structure of http requests for my whole system, and the data
	is dumped from the requests object of mitmproxy, normally, the following data is required to 
	describe a basic http request:

		request_time: 156979732
		src_ip: 192.168.1.1
		dst_ip: 192.168.1.2
		cookie: {"PHPSESSID":"aaa","my_id":"bbb"}
		headers: {"User-Agent":"curl/7.54.0"}
		response_code: 200
		response_length: 9176
		
	'''

	def __init__(self,request,response):
		self.request_time = time.time()
		self.src_ip = request.host
		self.dst_ip = request.host
		self.path = request.path
		self.content = request.content.decode('utf8')
		self.response_code = response.status_code
		self.response_len = len(response.raw_content)
		self.real_session = -1
		self.js_fp = 'unknown'
		self.method = request.method
		self.request = request
		self.response = response
		self.crawler = 'normal'   # normal / crawler / malicious crawler
		self.real_session = ''
		self.my_session = ''
		self.black = ''
		self.dynamic_info = ''

		# ctx.log.info(self.request.path)
		

		self.headers = {}
		tmp  = request.headers
		for header in tmp:
			self.headers[header] = request.headers[header]

		# Since we have the stand-alone field for Cookie, then no more cookie in headers
		if 'Cookie' in request.headers:
			del self.headers['Cookie']

		self.cookies = {}
		tmp = request.cookies
		for cookie in tmp:
			self.cookies[cookie] = request.cookies[cookie]


		self.query_dict = dict(parse_qsl(urlparse(self.path).query))

		# update the js_fp accordinfg to the cookie
		if 'js_fp' in self.cookies.keys() and self.cookies['js_fp']:
			self.js_fp = self.cookies['js_fp']

		# update the src_ip according to the fakeip
		if 'fakeip' in self.query_dict.keys():
			self.src_ip = self.query_dict['fakeip']

		# real session is used to label the real session key of the crawler, which will be used for contray to the session classification results
		if 'real_session' in self.query_dict.keys():
			self.real_session = self.query_dict['real_session']
			if 'real_session' not in self.cookies.keys():
				self.response.headers['Set-Cookie'] = 'real_session=' + self.real_session

		# real session may appear in cookies		
		if 'real_session' in self.cookies.keys() and self.cookies['real_session']:
			self.real_session = self.cookies['real_session']

		if not self._block_by_ip():

			if '/get_dynamic.html' in self.request.path:
				self.get_dynamic_payload()
			elif '/my_info.php' in self.request.path:
				self.collect_dynamic()
			elif '/taint_link' in self.request.path:
				self.taint_link()
			# if there is no js_fp in the cookie, then add the get_dynamic.html to the responses	
			elif  ('js_fp' not in self.cookies.keys() or not self.request.cookies['js_fp']):
				self.get_dynamic()

		# to reduce the pressure at the proxy, we migrate all the operation to handler
		self.request = {"request_time":self.request_time, "src_ip":self.src_ip, "dst_ip":self.dst_ip, "path":self.path, "content":self.content, "cookie":self.cookies, "headers":self.headers, "response_code":self.response_code, "response_len":self.response_len,"real_session":self.real_session, "js_fp":self.js_fp,"method":self.method, "crawler":self.crawler,"black":self.black,"query_dict":self.query_dict,"dynamic_info":self.dynamic_info} 

	def taint_link(self):
		'''
		This function is used to detect the taint link and label the request as crawler 
		'''
		self.crawler = 'crawler'
		self.response.text = 'This is a taint link'
		self.response.set_state({"status_code":200, "reason":b"OK"})

	def _block_by_ip(self):
		'''
		if ip is black, then generate payload accoring to its info

		'''
		if self.src_ip in config.black_ips.keys():
			ctx.log.error('black ip detected: ' + self.src_ip)
			self.black = 'black'
			self.crawler = 'malicious crawler'
			payload(config.black_ips[self.src_ip],self.response)
			return True
		else:
			return False


	# def check_static(self):
	# 	if self._check_crawler_by_method() or self._check_crawler_by_path() or self._check_crawler_by_ua() or self._check_crawler_by_refer() or self._check_crawler_by_headers_count():
			
	# 		self.crawler = 'crawler' 

	# def _block_by_session(self):
	# 	if self.my_session and self.my_session in config.black_sessions:
	# 		if not self.src_ip in config.black_ips:
	# 			config.black_ips = insert_black_ip(config.r,self.src_ip,config.all_sessions[self.my_session])
	# 		ctx.log.error('black session detected: ' + self.my_session)
	# 		return payload(config.all_sessions[self.my_session],self.response)
	# 	return False

	# def _block_by_ip(self):
	# 	if self.src_ip in config.black_ips.keys():
	# 		ctx.log.error('black ip detected: ' + self.src_ip)

	# 		if self.my_session in config.all_sessions:
	# 			if self.my_session not in config.black_sessions:
	# 				# add black session to global variables
	# 				config.black_sessions = insert_black_session(config.r,self.my_session,config.all_sessions[self.my_session])

	# 			return payload(config.all_sessions[self.my_session],self.response)
	# 		else:
	# 			# using random payload
	# 			return payload({"webdriver":"phantomjs"},self.response)
	# 	return False


	# def _check_crawler_by_refer(self):
	# 	return False

	# def _check_crawler_by_method(self):
	# 	if self.request.method not in config.allow_method:
	# 		return True
	# 	return False

	# def _check_crawler_by_path(self):
	# 	if self.request.path in config.crawler_path:
	# 		return True
	# 	return False

	# def _check_crawler_by_ua(self):
	# 	if not 'User-Agent' in self.request.headers.keys():
	# 		return True
	# 	for crawler_string in config.crawler_strings:
	# 		if crawler_string in self.request.headers['User-Agent']:
	# 			return True
	# 	return False

	# def _check_crawler_by_headers_count(self):
	# 	if len(self.request.headers) <= config.crawler_headers_count:
	# 		return True
	# 	return False


	
	# using the re will consume lots of resource, not suggested to be used here
	# def _check_malicious(self):
	# 	content = self.request.url + '|||' + self.request.raw_content
	# 	ctx.log.warn(config.malicious_rules)

	def get_dynamic(self):
		# insert the get_dynamic html to the response
		self.response.set_state({"status_code":200, "reason":b"OK"})
		html = BeautifulSoup(self.response.content, "html.parser")
		if html.body:
			iframe = html.new_tag(
				"iframe",
				src= '/get_dynamic.html',
				frameborder=0,
				height=50,
				width=1000)
			iframe2 = html.new_tag(
				"iframe",
				src= '/taint_link',
				frameborder=0,
				height=50,
				width=1000)
			html.body.insert(0, iframe)
			html.body.insert(0, iframe2)
			self.response.content = str(html).encode("utf8")

	def get_dynamic_payload(self):
		# get_dynamic.html in path
		# this function is used to return with get_dynamic payload
		self.response.set_state({"status_code":200, "reason":b"OK"})
		self.payload = open('payload/get_dynamic.html').read()
		self.response.headers['Content-Type'] = 'text/html'
		self.response.text = self.payload

	def collect_dynamic(self):
		# my_info.php in path
		# this function is used to collect the dynamic information from broswer
		self.response.set_state({"status_code":200, "reason":b"OK"})
		if 'info' in self.query_dict.keys():
			self.dynamic_info = base64.b64decode(self.query_dict['info']).decode('utf-8')
			self.response.text = "<html>success!</html>"
		else:
			self.response.text = "<html>hacker!</html>"

	def __dump__(self):
		pass

	def __repr__(self):
		pass

	def __str__(self):
		return json.dumps(self.request)



def my_log(msg):
	pass
	# open(config.my_log_file,'a').write(str(msg) + "\n")
	# ctx.log.info(msg)

def request(flow: http.HTTPFlow) -> None:
	pass    


def load_from_db():
	# this function will load data from redis database every n requests
	# config.black_ip['172.17.0.1'] = {'webdriver':'phantomjs'}

	config.black_ips = get_black_ip(config.r)
	ctx.log.info('loading black rules')


def response(flow: http.HTTPFlow) -> None:

	m = my_request(flow.request,flow.response)
	insert_request(config.r, m.__str__())
	my_log(m)


	config.request_count += 1
	n = 10
	if config.request_count % 10 == 0:
		load_from_db()
