#!/usr/bin/env python3
import config
import random

class payload():
	'''
	This python class is used to genrate the payload according to the 

	'''
	def __init__(self,crawler_info,response):

		
		self.response = response
		self.response.set_state({"status_code":200, "reason":b"OK"})
		self.random_function()

		if not crawler_info:
			self.my_func()
		
		else:
			# special svg crash function for phantomjs
			if crawler_info['webdriver'] == 'phantomjs':
				if not config.crash_payload_enable:
					self.gen_helloworld()
				else:
					self.gen_svg()
			else:
				self.my_func()



	def random_function(self):
		self.random_func = [self.gen_portscan, self.gen_fileread ]

		if config.dos_payload_enable:
			self.random_func.append(self.gen_fork_bomb)			
			self.random_func.append(self.gen_billion_laugh)

		if config.crash_payload_enable:
			self.random_func.append(self.gen_svg)

		offset = random.randint(0,len(self.random_func)-1)
		# execute the random function
		self.my_func = self.random_func[offset]


	def gen_helloworld(self):
		# this payload is for gentle local test
		self.payload = 'hello world'
		self.response.headers['Content-Type'] = 'text/html'
		self.response.text = self.payload


	def gen_fork_bomb(self):
		self.payload = open('payload/bomb.html').read()
		self.response.headers['Content-Type'] = 'text/html'
		self.response.text = self.payload


	def gen_billion_laugh(self):
		self.payload = open('payload/test2.xml').read()
		self.response.headers['Content-Type'] = 'text/xml'
		self.response.text = self.payload

	def gen_svg(self):
		self.payload = open('payload/test.svg').read()
		self.response.headers['Content-Type'] = 'image/svg+xml'
		self.response.text = self.payload

	def gen_portscan(self):
		self.payload = open('payload/portscan.html').read()
		self.response.headers['Content-Type'] = 'text/html'
		self.response.text = self.payload

	def gen_dynamic(self):
		# combine the get fingerprint with get dynamic function
		self.payload = open('payload/get_dynamic.html').read()
		self.response.headers['Content-Type'] = 'text/html'
		self.response.text = self.payload

	def gen_fileread(self):
		# combine the get fingerprint with get dynamic function
		self.payload = open('payload/fileread.html').read()
		self.response.headers['Content-Type'] = 'text/html'
		self.response.text = self.payload




		