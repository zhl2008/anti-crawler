#!/usr/bin/env python3
import redis

# init redis connection pool
r = redis.StrictRedis(host='172.16.10.1', port=6379, decode_responses=True)
_id = 0
my_log_file = './proxy.log'
black_ips = {}
black_sessions = {}
all_sessions = {}
request_count = 0
malicious_rules = [x for x in open('./malicious_rules.txt').readline() if x]
allow_method = ['GET','POST']
crawler_path = ['/robots.txt']
crawler_headers_count = 4
crawler_strings = ['baiduspider','tsinghuaspider','yisouspider','360spider','sogou web spider','bytespider','magibot','coccocbot','semrushbot','piplbot','bingbot','googlebot','cliqzbot','exabot','ahrefsbot','yak','feeddemon', 'indy library', 'alexa toolbar', 'asktbfxtv', 'ahrefsbot', 'crawldaddy', 'coolpadwebkit', 'java', 'feedly', 'universalfeedparser', 'apachebench', 'microsoft url control', 'swiftbot', 'zmeu', 'obot', 'jaunty', 'python-urllib', 'lightdeckreports bot', 'yyspider', 'digext', 'httpclient', 'heritrix', 'easouspider', 'ezooms', 'headless', 'curl', 'wget','bot','spider','cfnetwork','python','crawler','go-http-client','ruby']



dos_payload_enable = True
crash_payload_enable = True
