
#!/usr/bin/env python3

import config
import json

def insert_request(r, request_data):
    _id = get_request_id(r)
    if not _id:
        _id = 0
    else:
        _id = int(_id)
    inc_request_id(r)
    r.hset('my_request',_id + 1,request_data)
    return str(int(_id)+1)


def get_request_id(r):
    return r.get('request_count')

def inc_request_id(r):
    r.incr('request_count',amount=1)

def select_request(r,_id):
    return r.hget('my_request',_id)


def get_black_ip(r):
    black_ips = r.get('black_ips')
    if not black_ips:
        return {}
    return json.loads(black_ips)

def insert_black_ip(r,black_ip,black_info):

    black_ips = get_black_ip(r)
    black_ips[black_ip] = black_info
    r.set('black_ips',json.dumps(black_ips))
    return black_ips


def get_black_session(r):
    black_sessions = r.get('black_sessions')
    if not black_sessions:
        return {}
    return json.loads(black_sessions)

def insert_black_session(r,black_session,black_info):
    black_sessions = get_black_session(r)
    black_sessions[black_session] = black_info
    r.set('black_sessions',json.dumps(black_sessions))
    return black_sessions

def get_all_session(r):
    all_sessions = r.get('all_sessions')
    if not all_sessions:
        return {}
    return json.loads(all_sessions)

def insert_all_session(r,all_session,session_info):
    all_sessions = get_all_session(r)
    all_sessions[all_session] = session_info
    r.set('all_sessions',json.dumps(all_sessions))
    return all_sessions




if __name__ == '__main__':
    r = config.r
    # print(get_request_id(r))
    # insert_request(r,'{"request_id": 9, "request_time": 1561830418.6509552, "src_ip": "172.16.10.1", "dst_ip": "172.16.10.1", "path": "/aaa", "content": "", "cookie": {}, "headers": {"Host": "172.16.10.1", "User-Agent": "curl/7.58.0", "Accept": "*/*"}, "response_code": 200, "response_len": 15442}')
    # print(select_request(r,'4'))
    print(get_black_ip(r))
    insert_black_ip(r,'172.16.10.1',{"webdriver":"phantomjs"})
    print(get_black_ip(r))

    print(get_black_session(r))
    insert_black_session(r,'4eae91ee9f8da90fffe004acd6c4fd23',{"webdriver":"phantomjs"})
    print(get_black_session(r))

    print(get_all_session(r))
    insert_all_session(r,'4eae91ee9f8da90fffe004acd6c4fd23',{"webdriver":"phantomjs"})
    print(get_all_session(r))



