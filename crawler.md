# crawler

The article is associated with how to create the crawlers, including some technique details I confronted with when I establish the environments.


### selenium install and example

pip install -U selenium


```python
from selenium import webdriver

browser = webdriver.Firefox()
browser.get('http://seleniumhq.org/')
```

**Chromium and chrome-driver solution**

where to find the chromium binary:

https://commondatastorage.googleapis.com/chromium-browser-snapshots/index.html
for linux standalone binaries. choose linux_rel

download chrome-driver (it's strongly recommended that the chrome-driver and the chromium have the related version)

```python
from selenium import webdriver

options = webdriver.ChromeOptions()
options.binary_location = '/root/test/chrome-linux/chrome'
options.add_argument('headless')
options.add_argument('no-sandbox')
options.add_argument('window-size=1200x600')

browser = webdriver.Chrome(executable_path='/root/chromedriver',chrome_options=options)
browser.get('http://172.16.10.1:8002/')
browser.get_screenshot_as_file('main-page.png')
```


 **firefox and geckodriver solution**


wget https://github.com/mozilla/geckodriver/releases/download/v0.11.1/geckodriver-v0.11.1-linux64.tar.gz




### my crawlers based on different policy and other mature crawler at github


5 different data targets: 
    a. specific html data at superficial level
    b. website site structure at superficial level (BFS && DFS)
    c. special information in deep level ( with login or some token required)
    d. malicious scanner
    
8 different camouflage policy combinations:
    a. basic header element obfuscation (user-agent)
    b. IP proxies 
    c. execute the dynamic javascript
    
    
5 different request frequencies:
    a. slow
    b. medium
    c. quick
    d. extreme
    e. random
    
3 different web driver:
    a. phantomJS
    b. chromium
    c. firefox
    
    
    5*8*5*3 = 600
    10 types of normal crawler
    
    
    
### selenium grid

use the selenium grid for different types of browser
    
    
    
  
