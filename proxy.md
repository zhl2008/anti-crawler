# proxy design and implementation 


### motivation

why the proxy module is required?

* The anti-crawler system is supposed to be abled to deployed in different web application environments, which are always built and deployed under different OS architectures and implemented with different programing languages. Therefore, I introduce the scalable proxy module to intercept all the traffic outside. Proxy is believed to be the simplest way for implementation.

* I need to inject some JS script to probe the client-side network, and even exploit them with that JS, the proxy will provide a convenient method for script injection

* Some other modifications to both request and response


### recommended software

mitmproxy 

documentations and references of mitmproxy
https://mitmproxy.org/




### format data structure


session_id: 1
session_seq: 1
request_time: 1567322423
src_ip: 127.0.0.1
src_port: 63444
cookie_id: 
URL: /index.php?id&username&passwd
headers: {"user-agent":"google robots","Cookies":"xxxxx"}
client_signature: xxxxx
response_status: 200



{"request_time": 1561956984.3283665, "src_ip": "172.16.10.1", "dst_ip": "172.16.10.1", "path": "/plus/qrcode.php?action=get_qrcode&type=index&id=0", "content": "", "cookie": {}, "headers": {"Host": "172.16.10.1", "Connection": "keep-alive", "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) HeadlessChrome/70.0.3509.0 Safari/537.36", "Accept": "image/webp,image/apng,image/*,*/*;q=0.8", "Referer": "http://172.16.10.1:8001/plus/qrcode.php?id=0&type=index", "Accept-Encoding": "gzip, deflate"}, "response_code": 200, "response_len": 420}



the basic dependency: http request and response parse

### data store and rules load

Using the redis as the backend's database.

how to set up the redis data structure

1.request table {request_id request_data }
2.session {sess_id, request_id, fingerprint}
3.crawler {sess_id, crawler_data}

sess_id request_id
sess_id crawler_data
sess_id fingerprints
request_id request_data
request_id fingerprint


redis database:
my_request request_id request_data
my_fingerprint request_id fingerprint
my_session_fp sess_id fingerprint [[][][]]

my_session_request sess_id request_ids



my_sess sess_id  request_ids


fingerprint matching algorithm:

fingerprint = ip_fp + dynamic_request_data_fp +  rtc_fp + other_fp

find common part in two fingerprint,
if xxx, then the request matches 






### http pressure test

apache benchmark



$$score = \frac {W_{ua} * M_{ua} + W_{url} * M_{url} + W_{other} * M_{other} + W_{order} * M_{order}} {C_{field} + 2}$$


$$I(F(x)=f_n)=-log_2(P(fn))\tag{1}$$

$$I_s(f_{n,s})=-log_2(P(f_{n,s}))$$

$$H_s(F_s)=-\sum_{n=0}^{N}P(f_s,n)log_2(P(f_s,n))\tag{3}$$





### mitmproxy shortcuts

Flow List (initial view)
Key	Function
enter	view flow (go to Flow View)
shift + e	toggle eventlog
tab	tab between eventlog and flow list
o	set flow order, will ask for sort criteria - useful for finding and deleting large download flows before saving stream
d	delete flow
f	filter view
w	save flows
z	zap (clear) flow list or eventlog
q	quit mitmproxy
Flow View (single flow)
Key	Effect
tab	next tab (cycles through Request - Response - Detail)
w	save all flows matching current view filter
f	load full body data
space	next flow
/	search (case sensitive)
n	repeat search forward
N	repeat search backwards
q	return to flow list




### 基本handler算法

规则库中存储的是ip黑名单和session黑名单

对于request的处理部分将全部在获取到response后进行
proxy处对request的处理：





```py

def block_by_session(request):
    if request.session_key and request.session_key in session_black:
        if not request.src_ip in ip_black:
            ip_black.append(request.src_ip)
        return gen_payload(request.session_key)
    return False
    

def block_by_ip(request):
    if request.ip in ip_black:
        if request.session_key in all_session:
            if request.session_key not in session_black:
                session_black.append(session_key)
            return gen_payload(session_key)    
        else:
            return gen_random_payload()
    return False
            
            
if not (block_by_ip or block_by_session):
    if 'js_fp' not in response.header.cookie
        return gen_payload('js_fp')
save_info(info)
    
    
```
 
 
 1. black session 以及 black ip的污染传播算法
 2. 静态检查：主要通过单粒度的爬虫检测规则来标注相应的爬虫信息
 3. 数据存储：所有request的关键信息存储到redis数据库中
 
 proxy中单个request处理伪代码
 ```py
 Data: session_key, src_ip, cookies
 Result: generate proper payload
 
 init black_sessions;
 init black_ips; 
 init all_sessions;
 if session_key is valid and session_key in black_sessions then
        if request.src_ip not in black_ips then
            black_ips append src_ip
        /* function gen_payload will generate payload according to session_key*/
        end
        gen_payload(session_key)
               
elif src_ip in black_ips then
    if session_key in all_sessions then
        if session_key  not in black_sessions then
            black_sessions append session_key
        end
        gen_payload(session_key)
    else:
        /* generate random payload */
        gen_payload(random)
    end
end
if 'finger_print' not in cookies then
    gen_payload(fp_generator)
end

/* this function will check the request at the request granularity */
check_static()
/* save all the information to database */
save_info()

    

   
end     

```
 
 handler处对request的处理
 
 
session分类算法     
```python

for request in all_requests:
    # dynamic
    if request.has_key(js_fp):
        if js_fp not in dynamic_session_group:
            dynamic_session_group[js_fp] = [request]
        else:
            dynamic_session_group[js_fp].append(request)
        
    # static
    else:
        for session_group in static_session_group:
            if calc_similarity(request,session_group) < threshold:
                generate new_session_group
                static_session_group[new_session_group] = [request]
            else:
                 static_session_group[session_group].append(request)
    
```  



session特征提取和恶意爬虫识别
```python
all_session_group = static_session_group + dynamic_session_group
for session_group in all_session_group:
    # single granularity
    for request in session_group:
        if crawler_check(request.crawler):
            session_group.crawler =  "crawler"
        elif dynamic_check(request)
            session_group.crawler = "crawler"
        if malicious_check(request):
            session_group.crawler = "malicious crawler"
    
    session_group.crawler = session_check(session_group)

```
伪代码：
```python
Data: static_session_group, dynamic_session_group
Result: crawler info in all_session_group
Initialization;
all_session_group = static_session_group + dynamic_session_group
for session_group in all_session_group:
    # single granularity
    for request in session_group:
        if crawler_check(request.crawler):
            session_group.crawler =  "crawler"
        elif dynamic_check(request)
            session_group.crawler = "crawler"
        if malicious_check(request):
            session_group.crawler = "malicious crawler"
    
    session_group.crawler = session_check(session_group)

```




 
 静态session 分类

 
 ```python
 
 
 
 
 ```       







  
        
    
需要提取的实验评估效果


单粒度下的爬虫识别效果

总共的session数目
总共的请求数目




crawler 加上单粒度识别的标记

crawler上的real session
 
 
 
 crawler 单粒度识别效果
 crawler session静态分类+动态分类 分类效果
 crawler的session识别效果
 crawler的动态信息识别效果
 性能损耗评估： 请求处理速度，平均响应时间
 
 
 crawler的恶意爬虫捕捉演示 （捕捉到的恶意爬虫及其相应的请求）
 crawler的阻断效果演示 （恶意的phantomjs的爬虫）
 crawler的敏感信息窃取演示




 
 
 
 