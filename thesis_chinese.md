# thesis (chinese version)


### 论文工作计划

1.论文研究目标

机器学习，数据挖掘等大量数据依赖型的技术在高速发展的同时，对于相关数据的质和量都提出了更高的要求。网络爬虫，又称网络蜘蛛或者网络机器人，在这样一个数据消费的时代，扮演着数据搬运者和传递者的角色。在其运行的生命周期中，往往会按照开发者预设的规则，爬取指定的URL地址或者URL地址列表，并将获取到的数据预处理成标准化的格式[2]。

普通的网络爬虫并没有危害，相反，搜索引擎存储数据的来源都是基于大量分布式网络爬虫爬取得到的结果，网络爬虫在数据传递过程中起到至关重要的重要。但是多线程高并发的失控爬虫，针对网站隐私数据的窃密爬虫，以及不遵守爬虫道德规范的恶意爬虫，都对网络空间健康的生态环境提出了巨大的挑战。恶意爬虫通常给网站的性能资源和带宽资源带来巨大的消耗，甚至会影响正常人类用户的体验，这样的爬虫对网站的影响相当于是DDOS攻击[1]。

本文提出了一种新型的针对恶意爬虫的检测和攻击的对抗性技术，用于在爬虫实时访问阶段，对爬虫进行实时监测与阻断，并针对识别出的爬虫中具有恶意探测行为和攻击行为的爬虫，在检测其相应的webdriver的类型和版本，生成相对应的攻击载荷。

目前国内外已经针对爬虫识别方法提出了多种方案，在针对普通爬虫的识别上，能够实现较好的检测效果,但仍存在不足之处。xxx,xxx,xxx 这些方法都局限于从web访问日志中去抽取爬虫的特征，并不能充分利用爬虫在http字段中表现出的特征。xxx提出了在session粒度上实现爬虫识别，但是其只考虑到普通爬虫的流量，对于存在伪装与对抗行为的恶意爬虫识别效果不佳。xxx训练了马尔可夫链的模型来判断，但使用的数据集过于老旧，无法适应新型的和具有对抗行为的恶意爬虫。在恶意爬虫反击部分，我们需要借助传统的二进制程序漏洞挖掘技巧来发掘可以利用的漏洞，用于扰乱恶意爬虫的访问与探测。

恶意爬虫识别与攻击系统的目标功能为：1）捕获到目标网站的所有访问请求，并实时地将其按照session粒度进行分类；2）通过不同粒度下的数据分析，检测和识别HTTP请求中出现的爬虫；3）根据设定阈值，从爬虫session中筛选出恶意行为的爬虫；4）利用事先生成的攻击载荷攻击恶意爬虫，并收集相应攻击结果。



###论文主要研究内容

主要的研究类容包括以下几部分：
a. 对抗样本下session分类方法的研究
在普通的爬虫和正常的人类访问行为下，针对给定的http request序列进行session分类并不是一个困难的问题。往往我们在某一特定时间段内，将同一ip来源或者使用同一session的http request分类到同一个session，但是在存在对抗行为的恶意爬虫面前，这样粗糙的分类方式通常难以获得令人信服的分类结果。参考利用http请求的静态特征以及利用javascript执行得到的动态特征，以此得到一个更加精确的session分类结果，是我们研究关注的重要内容之一。此外，针对一个真实生产环境下收集的实时数据，如何实现实时的session分类处理，以及各类参数和阈值（session分类的时间长度和序列长度）的合理设定，也是我们研究的一部分。

b. 基于session粒度的爬虫识别与检测
传统基于单个http request的爬虫识别方法，仅仅基于一些http字段的静态特征，很容易使用一些额外的工具和算法对http字段进行变异，从而逃避传统反爬虫程序的检测。因此，我们考虑到从session中额外提取爬虫的特征，并参考原先的静态特征，共同作用于爬虫识别。目前已经有一些论文讨论到基于session的爬虫识别，并给出了相关了session粒度下的爬虫特征，但是这些文章提出的一些特征存在一些问题：
* 一部分特征实时检测的性能开销过大；
* 一部分特征在数据集中并不显著，或者对恶意爬虫的支持不佳；
因此，通过本次研究，我们还会得到一系列session的特征以及这些特征的相关阈值设定。

c. 基于隐式浏览行为的爬虫识别
正常的人类浏览行为下，除了访问频率和访问间隔于爬虫存在差异之外，还会存在一下隐式的浏览行为。例如在相关页面插入一些不在浏览器上渲染但是存在html代码中的链接，在正常访问下不会被触发，但是遇到了爬虫的html link parser，则可能会被访问，由此我们可以判定访问该链接的请求对应的session均是来自于爬虫的。如何能够利用人类隐式的浏览行为，来进行更为精细准确的爬虫识别，也是本研究的重点。

d. 针对爬虫webdriver的自动化漏洞挖掘技术
一旦识别出爬虫(或者恶意爬虫），在此基础使用javascript探测其webdriver版本以及可能存在的漏洞类型，并通过此漏洞生成相应的攻击载荷实现对爬虫的攻击，这是我们系统的最终的目的。如何通过已知的webdriver版本，来生成有效的攻击载荷，我们需要借助于已经成熟的自动化漏洞挖掘技术。虽然单个的webdriver程序体积很大，但是因为我们目标的webdriver往往是开源的。因此，本课题将研究在在获得源代码的前提下，如何对程序功能进行分割或者采用一些其他的简化技术，预防可能出现的路径爆炸问题，来尽可能多地找到一些漏洞。



### 已经完成的工作
a.多策略爬虫工具与原始数据收集/分析

为了检验和评估不同的方案下，session分类算法以及爬虫识别算法的检测率和效率，我们使用了三个不同的数据集进行评估。前两个数据集分别为：
* 清华大学某网站访问数据集 （正常用户访问 + 爬虫访问）
* 360天眼恶意攻击流量数据集 （对抗样本）
为了模拟真实网络环境中的爬虫请求，我们设计了一系列的爬虫策略，并基于这些策略，生成了近千种不同类型的网络爬虫。我们考虑到的爬虫特性有：
* 爬虫的目标数据
* 爬虫的请求速率
* 爬虫的搜索算法
* 爬虫的驱动程序
* 对抗性的爬虫策略（字段变异，ip伪装）
这些不同类型的爬虫将会产生大量普通的爬虫请求以及具有对抗性的爬虫请求，这些爬虫请求在产生的过程中会记录session id用以检测session分类的效果，并且这些爬虫均具有能够执行服务反馈的javascript的特性，因此可以用于收集不同webdriver下的动态指纹，这些爬虫请求将会作为本研究的第三个数据集。
通过基于user-agent的传统方法针对数据集a进行清洗，我们发现基于ua的方法识别出请求中的24.3%为来自各类爬虫的请求，爬虫的组成成分主要是来自于各大搜索引擎的爬虫以及专门针对清华网站的爬虫，如图xxx所示。因为该数据集几乎不存在对抗样本，因此我们使用ip作为session分类的标准，对全部的10w个请求进行session分类，在去除一些超长和超短的session之后，我们可以得到了以下的session的长度分布。



b.session分类与session分析
在session分类的处理上，本研究首先采用http字段静态特征作为session分类的依据。主要采用的静态特征有：User-agent字段，URL请求路径，其余header字段，header字段键与其排列顺序。引入header字段排列的主要是基于这样的一个直觉：如果目标爬虫使用了http header字段变异的对抗行为，虽然会改变字段具体的值，但可能忽略对字段键的变异，给该特征赋予较高的权重，能够有效地在此类对抗行为下正确地分类session。在计算两个http请求的相似度时，我们采用如下的计算函数:

$$score = \frac {W_{ua} * M_{ua} + W_{url} * M_{url} + W_{other} * M_{other} + W_{order} * M_{order}} {C_{field} + 2}$$

此处的$W$为权重相关的系数，$M$表示相关字段是否匹配，匹配取值为1，反之取值为0,$C_{field}$表示相关字段的数目，用以避免字段过多的请求出现评分过高的情况。
此外，我们额外设置相应的阈值，一旦相似度超过该阈值，我们即可将两个http请求归类为同一个session。我们在数据集a上测试我们的算法，并使用梯度下降的方法，求得这些参数的较优解。下图是我们使用该静态分类方法效果图（梯度下降图），我们最终采用xxxx这样的参数取值，其分类准确率可以达到90%。

对于能够处理服务器返回的javascript的爬虫，我们使用canvas指纹/javascript引擎指纹等一系列浏览器追踪技术来生成其动态指纹，根据xxx的论文，动态指纹的独特性和熵能够得到保证。因此，在并发流量不太高的情况下，将其作为session分类的重要依据，能够得到相当高的准确率。我们将动态指纹加入到我们的相似度计算函数，函数更新后为：
$$score = \frac {W_{ua} * M_{ua} + W_{url} * M_{url} + W_{other} * M_{other} + W_{order} * M_{order} + W_{dynamic} * M_{dynamic}} {C_{field} + 3}$$

我们使用数据集c来作为评估基于静态/动态指纹的session分类方法的准确性，最终结果显示，我们的准确率是xxx，而静态指纹的session分类算法在数据集c上的准确率仅仅为xxx。



c.爬虫指纹数据库与爬虫检测规则
一般的爬虫在其user-agent字段上都会有明显的特征，我们收集了一些常见爬虫的关键字，作为http request粒度上爬虫识别的参考。这些关键字如下所示：
['baiduspider','tsinghuaspider','yisouspider','360spider','sogou web spider','bytespider','magibot','coccocbot','semrushbot','piplbot','bingbot','googlebot','cliqzbot','exabot','ahrefsbot','yak','feeddemon', 'indy library', 'alexa toolbar', 'asktbfxtv', 'ahrefsbot', 'crawldaddy', 'coolpadwebkit', 'java', 'feedly', 'universalfeedparser', 'apachebench', 'microsoft url control', 'swiftbot', 'zmeu', 'obot', 'jaunty', 'python-urllib', 'lightdeckreports bot', 'yyspider', 'digext', 'httpclient', 'heritrix', 'easouspider', 'ezooms', 'headless', 'curl', 'wget','bot','spider','cfnetwork','python','crawler','go-http-client','ruby']

通过借鉴其他论文中已有的特征，并根据本研究的数据集中一些特征的特性（显著水平，计算效率），我们使用如下的特征组作为我们在session粒度上判定爬虫的依据：
* 4xx 返回状态码的比例
* 5xx 返回状态码的比例
* 某时间段内（如1min）内的最大请求次数
* 请求时间间隔方差
* 请求URL资源不同类型比例（css，js，image，html）
在完成session分类后，我们从每一个session中尝试抽取以上特征。一个某个session的特征能够满足事先设定的阈值，我们就可以将该session视为是来自于爬虫的请求。一个典型的爬虫session与人类访问的特征比较如下图所示：

| 特征 | 人类session | 爬虫session | 恶意爬虫session |
| ------- | ---- | --------- | -------- |
|4xx比例| 1.6% | 7.1% | 6.7% |
|5xx比例| 0.5% | 1.6% | 6.1% |
|最大请求| 5    | 13   | 50 |
|时间方差（s)| 155.67| 61.80 | 90.74|
|page资源比例| 7.8% | 100% | 100% |

除了通过request粒度和session粒度进行爬虫识别外，我们还注意到人类的隐式浏览行为可以被用于鉴别爬虫，由此设计了额外的爬虫检测规则。该检测规则主要由两部分组成：
* 通过插入一些javascript尝试捕捉人类的点击，光标移动以及键盘行为，如果长时间不存在这样的行为，则可以判定为该对应session来自于爬虫；
* 通过在正常的页面中插入污点页面（taint page），污点页面中存在污点链接（taint link)，该链接在html渲染时不会显示出来，但可能会被爬虫的link parser所识别，并产生相应的访问请求，我们把存在该类请求的session标记为爬虫。



d.恶意爬虫对抗系统的基本实现

恶意爬虫对抗系统主要由如下四个部分组成：
proxy: 使用mitm-proxy实现的反向代理，转发所有流量到指定服务器，记录所有请求到redis 数据库，并根据相应规则，在返回的数据中添加动态指纹识别脚本，或者针对恶意爬虫的攻击脚本；
server: 模拟的web服务，包含一定的目录结构以及深层次的信息，作为爬虫爬取的目标网站
crawler：多策略爬虫宿主系统，包含实验涉及的所有种类的爬虫
handler：包含用于存储用户请求的redis数据库，并对单个的raw request实现session分类，以及爬虫识别，并根据识别的结果制定相应的响应策略
四个主要部分的关系图如下所示：
xxx
该恶意爬虫的对抗系统的全部源代码将在gitlab上开源。


### 关键技术和难点
1. 浏览器动态指纹生成算法

目前，常见的浏览器动态指纹主要有：
* Canvas指纹：canvas元素是html5规范中新增的一类元素，能够在屏幕上可编程地绘制图像，并且被大部分主流的浏览器支持。Mowery和Shacham提出了通过HTML 5 的 Canvas API以及WebGL渲染文本得到像素差异，作为一种具有高信息熵的浏览器指纹生成方式[27]。
* WebRTC指纹：webRTC全称为web-based real-time communication。它在浏览器与浏览器或者设备与设备之间直接建立通信的信道，而不需要经由第三方服务器的转发[30]。尽管webRTC使用了具有高度高度安全性的传输协议，但是也引入了新的需要额外保护的敏感资源，如私有ip和内部网络。
* Javascript指纹：MULAZZANI等人提出了通过javascript的一致性测试的结果来区分不同浏览器，从而生成指纹的方法[28]。

这些指纹在不同的硬件，os以及浏览器上，具有很强的特异性，这些敏感信息可以用于追踪浏览器或者爬虫。

2. 基于静态/动态指纹的session分类
由于存在具有对抗性行为的爬虫，因此仅仅靠ip地址，或者网站cookie等简单方式得到的session分类结果往往是不可靠的。为了得到更为精确的session分类效果，我们需要充分利用我们从网站访问者获取的信息。这部分信息可以按照其特性分为静态指纹与动态指纹两部分，静态指纹主要依赖于http header的各种字段的取值、字段键的排列顺序以及url请求的资源模式，动态指纹主要利用一些浏览器的动态指纹生成算法，生成一些和访问者底层驱动相关的一些hash值。在指纹特征抽取的过程中，需要保证该指纹特征具有较强特异性，能够用于区分不同webdriver底层实现。

3. 爬虫检测与恶意爬虫识别
从单个request的字段特征，再到session粒度上抽取的特征，都将用于爬虫的检测。但是在保证这些的特征的准确性和有效性的同时，如何合理设置每个特征在判定过程的权重以及判定为爬虫的阈值，这是一个极为重要的步骤，它关系着本系统最终的爬虫识别效果。
此外，因为本研究所提出的攻击行为是针对于恶意爬虫的，我们并不想干预普通爬虫的爬取过程，或者只需要对于这些普通的爬虫做一些阻断即可。因此，如何在标注为爬虫的数据集中区分普通爬虫和恶意爬虫也是本研究的技术难点之一。

4. 符号执行与动态污点分析
符号执行是二进制动态分析中一种至关重要的技术。它的关键思想是使用符号值而不是固定值，用作程序或者函数的输入。并将程序中的变量表示成与符号输入相关的符号表达式。最终，程序输出的计算结果以符号输入的函数表示[32]。
动态污点分析的目的是在源和目的之间追踪信息流。程序中任何从污点数据计算得出的变量都被认为已污染（tainted），其他变量被认为未污染（untainted）。污染策略P决定污点在程序执行的过程中如何流动，什么样的操作会产生新的污点，以及针对已污染的数据会有什么样的检查。尽管污染策略会随着被分析的程序的不同而改变，但是基本的概念是一致的[33]。
符号执行技术主要用于在二进制程序漏洞挖掘过程中，尽可能地探索该程序的控制流路径，污点分析主要用于判断由符号执行搜寻出的执行路径是否存在控制流劫持的漏洞。我们将结合这两种二进制程序分析技术，来尝试对特定版本的爬虫driver的漏洞自动化挖掘。




### 下一阶段工作计划
1.未完成的工作

* 针对爬虫指纹的爬虫webdriver识别
通过收集的动态爬虫指纹信息，对爬虫底层所使用的webdriver进行识别，从而根据其webdriver具体的类型和版本信息，构造不同的攻击载荷。

* 爬虫底层漏洞自动化挖掘
使用上述的符号执行与动态污点分析技术，并充分利用可能开源的爬虫webdriver的源代码，来对爬虫的webdriver底层的接口进行fuzz，得到在特定场景下能够导致爬虫程序奔溃的非法输入，并进一步过滤，得到最终的攻击载荷。

* 恶意爬虫的行为策略与对抗策略
进一步研究恶意爬虫可能使用的对抗策略，完善恶意爬虫识别的相关策略。并强化现有的普通爬虫/恶意爬虫分类器，以得到更加精确的恶意爬虫检测效果，从而尽可能避免对普通爬虫和用户浏览器的”误伤“。


2.解决问题的技术思路或措施

针对爬虫webdriver识别的问题，我们可以基于指纹数据库，也可以将指纹数据库作为ML的训练样本，训练出一个具有较好的分类器。针对主流的爬虫底层驱动，通过漏洞自动化挖掘技术，发掘可能的攻击payload，为了避免可能存在的路径爆炸问题，可以依赖于源代码分割的方式减小程序的体量，也可以考虑引入一部分人工工作，来保证漏洞挖掘的质量。针对其他当前未发现的爬虫的对抗行为，可以收集一些现有的、较为著名的反反爬虫工具，研究他们的对抗策略，并通过相应的人工分析，抽取出它们的对抗策略。

3.下一阶段计划
2019.9.1 - 2019.9.30        分类器构造与漏洞自动化挖掘
2019.10.01 — 2019.10.31     系统完善与评估
2019.11.01 — 2019.12.31     撰写毕业论文，准备答辩



### 主要参考文献


1. crawler detection: A bayesian approach  (给出了基于ip和时间片进行session分类的策略，并给出了需要采集的session的features: 最大点击速率, session持续时间,静态文件请求, pdf文件请求, 4xx返回, robots request,使用bayesian网络进行训练)

2. Real-time web crawler detection ( apply 1 in a real-time system, 增加了一些新的特征，head 请求的数目， avg time)

3. An investigation of web crawler behavior: characterization and metrics

4. Classification of web robots: An empirical study based on over one billion requests (虽然其基于大量的数据，但主要基于不完整的流量日志，无法获取完整的信息）

5. Discovery of web robot sessions based on their navigational patterns（虽然基于session层次去尝试做爬虫识别，但是没有考虑到可能的恶意爬虫的对抗行为）

6. Web robot detection in scholarly Open Access institutional repositories （对于某一个数据集的爬虫识别的算法效果比较么，提出了大量的静态特征和动态特征）

7. Access Patterns for Robots and Humans in Web Archives （同时使用request的特征和session的特征，也提出了session分类的问题，但是session分类仅仅使用了ip以及ua特征）

8. Detecting web robots using resource request patterns （基于session的url请求类型进行爬虫识别，使用了markov chain model）

9. Classifying Web robots by Kmeans clustering （定义了session与session之间的距离，并使用了k-means来实现爬虫的分类）

10. Martin M (2012, June 29). Crawler user agent database. Social Media Today RSS. Retrieved from https://github.com/monperrus/crawler-user-agents/blob/master/crawler-user-agents.json




### 剩余的需要完成的实验

梯度下降进行session分类 ok
数据集c 加入动态指纹的session分类效果  (96.71%)
数据集b，c 使用静态指纹的session分类效果   （数据集b的静态分类准确率 35.67%，数据集c的静态分类效果23.73%）

人类，爬虫，恶意爬虫的session特征比较

最终的爬虫识别效果





[0.5,0.02,0.11,0.08,0.00,0.14]

添加了动态指纹
[0.5,0.02,0.11,0.08,0.00,0.00,0.14]

[0.5,0.02,0.11,0.08,0.00,1,0.2]


1.多策略爬虫工具与原始数据收集/分析
爬虫使用的类型种类：
a.爬虫的目标数据
b.爬虫的请求速率
c.爬虫的搜索算法
d.爬虫的驱动程序
e.对抗性的爬虫策略 （ip伪装，字段混淆）

数据样本：
a. 清华大学某网站访问数据集 （正常用户访问 + 爬虫访问）
b. 360天眼恶意攻击流量数据集 （对抗样本）
c. 多策略爬虫工具与常用爬虫工具访问数据集 （静态指纹 + 动态指纹可用）

数据样本a，用于静态爬虫规则制定与测试，用于基于session的爬虫识别 （session分类基于ip和时间片长度，传统静态方法） 

在数据集a下使用基于静态指纹的爬虫识别技术，分别在单个请求和session粒度上进行测试，得到的实验结果如下：

single request static recognition：
总共100000个请求， 其中识别出的爬虫数目为：
total: 24323

不同类型的爬虫请求次数分布如下所示：
piplbot: 572
semrushbot: 4586
googlebot: 1159
bytespider: 1505
360spider: 1170
yisouspider: 2005
cfnetwork: 3012
tsinghuaspider: 5762
sogou web spider: 107
baiduspider: 3309
other: 1136

session 分类：
因为因为请求数据截取问题，存在大量的只有单个请求的request，我们在分析过程中清洗掉这一部分数据，仅保留session长度>10的请求，我们可以得到如下的session


数据样本b，用于验证传统方法在对抗样本上的测试（基于ip的session分类出现问题，对于ip伪装的对抗策略束手无策）

数据样本c，用传统方法进行检测，用基于session的动态指纹算法进行检测，比较效果

baseline: 基于单个request的静态方法，基于流的静态方法
论证在存在对抗的情况下，静态指纹的有效性（传统基于cookie的session分类时，容易被篡改和伪造）

1.cookie赋值 （不接受cookie的浏览器默认拒绝访问）
2.生成指纹 （拒绝js执行的浏览器默认拒绝访问）
3.cookie与指纹的唯一绑定 （多个浏览器使用同一个指纹 一个浏览器使用多个指纹） using js obfuscation to protect your algoerithm from being cracked. the js obfuscation will be more secure than the traditional cookie approaches, although the absolute secrity is impossible.


crawler session规则设定
根据之前的文章总结出来的规则，但是规则的阈值如何设置（决策树？/手工设置？）

使用该session检测方法处理dataset a，分析处理结果（可能会出现fp，但也可能是真的postive，说明了session检测方法能够对于request粒度的检测方法，能够提供有效的补充。）

基于dataset a使用静态方法进行session分类，传统的session分类往往基于cookie，很多甚至由数据集直接提供session的标签。但在存在恶意爬虫的对抗环境下（1.cookie不可靠，2.一些字段存在变异，如sip），静态方法的效果并不是很好。

在dataset a下使用静态方法进行分类，效果尚可
在对抗数据集dataset b下使用静态方法进行session分类，效果不佳 （ex 对于恶意漏洞扫描器，payload使用的静态特征没有办法进行规范）

在dataset c下使用 1.request粒度下的爬虫识别 2.静态指纹下的爬虫识别 3.动态指纹下的爬虫识别，使用前两个作为基准，来评价第三种的效果。评价的参数：a.session分类效果 b.爬虫识别效果











2.session分类与session分析

session classification:  
1. based on ip, and sometimes have to adjust the time slot for session
2. my new method based on (ip, static fp, dynamic fp), which will provides a more precise result, and is not strongly depending on the session time slot
3. use the dataset c to testify the result of traditional session classification method and the method used in this thesis.

session analysis:
1. session feature extraction
2. crawler or human problem ( bayesian network? or other ML method?)





3.爬虫指纹数据库与爬虫检测规则
静态指纹数据库
动态指纹数据库
静态规则
动态规则
session规则


4.恶意爬虫对抗系统的基本实现










related work的不足之处：

constraint to the data (only part of the data due to privacy problems, or not representative enough in that specific field)

some of the related work based on session, however they only based on session, while not taking the information of the single requests into consideration

as for the session classification part, they use the bayesian method, which is based on the static method, I introduce the dynamic fingerprint for better classification


innovation: 
1. combine the single request and session analysis together for crawler detection
2. introduce the dynamic fingerprint for better classification



### oral defense

老师，同学们下午好！
我的指导老师是xxxx，我本次答辩的论文题目是：
恶意网络爬虫检测与对抗技术的研究及实现


论文工作计划：

在大数据时代的背景下，人们对于数据的需求量也越来越大，而大量的信息都被存储在web中。因此，人们常常借用网络爬虫来实现对目标站点，目标数据的爬取。

但是，网络中同样充斥着大量的恶意爬虫，他们或者高频率地访问目标网站，大量占用目标网站的带宽和处理资源，或者使用恶意的攻击载荷尝试入侵目标网站，或者窃取目标网站的敏感数据。

目前已经有很对针对爬虫检测识别相关的研究工作。但是这些工作仍然存在很多不足之处：
1.他们数据集过于老旧
2.他们仅仅使用了少量的访问数据特征，并没有充分利用访问数据中的信息
3.没有考虑到恶意爬虫的对抗行为，在恶意爬虫的识别上效果不佳

本文的研究目标即是研究恶意爬虫的检测与对抗技术，并基于该技术，构建一套有效的爬虫识别与攻击系统。

论文主要的研究内容有：
1.对抗样本下的session分类研究
前人的session分类是将某个固定时间片内统一ip的请求归类为一个session，但对于分布式爬虫而言，会出现很多的错误。本文的研究目标之一就是找到这样一个session分类方法，来实现针对存在的对抗行为的数据的session分类。

2.基于session粒度的爬虫识别和检测
传统的爬虫仅仅基于单个的请求，不能充分利用访问session中的信息。

3.基于隐式浏览行为的爬虫识别
人类在访问网页的时候，存在一些和爬虫不同的行为。比如说一些鼠标和键盘的操作，也不会访问浏览器没有渲染出来的链接。

4.webriver漏洞自动化挖掘
使用一些二进制漏洞自动化挖掘技术，来挖掘爬虫底层可能使用的webdriver的漏洞。


已完成的工作:

1.多策略爬虫工具与原始数据收集/分析
针对这个课题，我们总共收集到了三个数据集：
xxx
其中数据集a是一个包含大量普通爬虫，以及人类浏览数据的数据集
数据集b是360收集的一些的攻击流量，这些攻击流量往往采用了一些对抗的行为，普通的爬虫检测手段并没有很好的效果。数据集c我们研究了大量爬虫种类后生成的数据集，该数据集中不仅仅包含爬虫的静态访问特征，还包含了具有很高特异性的动态指纹特征。

2.session分类与特征提取
静态session分类：考虑到爬虫发送的HTTP请求中的静态特征，使用一定的公式可以计算两个请求之间的相似度，以此作为特征提取的依据。

动态特征主要依赖于使用javascript生成的一些指纹

我们使用了三个数据集来验证动态特征的可靠性：

3.爬虫指纹数据库与爬虫检测规则
我们收集了一些user-agent特征，以及session的一些特征，这些session的特征是：
xxx

4.恶意爬虫对抗系统的基本实现
主要由四部分组成


关键技术和难点：
1.浏览器动态指纹生成算法
常见的指纹有：xxx
这些指纹依赖于不同浏览器，不同OS,甚至是不同的硬件，产生的指纹也不尽相同，具有很高的特异性。

2.动态，静态指纹session分类：
对于具有对抗行为的爬虫，结合动态指纹和静态指纹来做识别。

3.爬虫检测和恶意爬虫识别：
我们拥有一个请求的静态特征，以及这个请求所在的session特征，为这些特征赋予合适的权重，用于判定，人类+爬虫+恶意爬虫

4.符号执行与动态污点分析：
符号执行用于探索路径，污点分析用于判断是否可攻击


下一阶段工作计划：
1.未完成的工作：
webdriver识别（不同的webdriver 我们会采用不同的攻击载荷）
底层漏洞自动化挖掘 (找到可以攻击的载荷）
恶意爬虫的行为策略与对抗策略 （更为准确地判定恶意爬虫）

2.解决问题的思路和措施：
webdriver识别 （建立指纹数据库，训练分类器）
底层漏洞自动化挖掘 （程序分割，人工干预）
未知对抗行为 （收集一些反反爬虫，研究其策略）







