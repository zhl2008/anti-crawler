#!/usr/bin/env python3


session_label_thread = 5
fp_update_thread = 1
redis_ip = '127.0.0.1'
redis_port = 6379
log_file = './my_log'
log_to_file = True
thread_watch_span = 60
crawler_detection_span = 30
gen_fp_thread_num = 1
gen_sess_thread_num = 1
handler_thread_num = 1

malicious_rules = [x.strip() for x in open('./rules.txt').readlines() if x]
allow_method = ['GET','POST']
crawler_path = ['/robots.txt']
crawler_headers_count = 4
crawler_strings = ['baiduspider','tsinghuaspider','yisouspider','360spider','sogou web spider','bytespider','magibot','coccocbot','semrushbot','piplbot','bingbot','googlebot','cliqzbot','exabot','ahrefsbot','yak','feeddemon', 'indy library', 'alexa toolbar', 'asktbfxtv', 'ahrefsbot', 'crawldaddy', 'coolpadwebkit', 'java', 'feedly', 'universalfeedparser', 'apachebench', 'microsoft url control', 'swiftbot', 'zmeu', 'obot', 'jaunty', 'python-urllib', 'lightdeckreports bot', 'yyspider', 'digext', 'httpclient', 'heritrix', 'easouspider', 'ezooms', 'headless', 'curl', 'wget','bot','spider','cfnetwork','python','crawler','go-http-client','ruby']


# dynamic crawler features
window_size_threshold = 100 * 100
closed_time_threshold = 100
dynamic_score = 2
static_weight = [1,0.02,0.11,0.08,0,0,0.04]
dynamic_weight = [0.5,0.02,0.11,0.08,0,20,1]


max_freq_time = 60
page_postfix = ['.html','.htm','.php','.asp','.aspx','.jsp','.jspx']
