#!/usr/bin/env python3

import os
import sys
import threading
import queue
import time
import log
import json
import config
import redis
import re
import random
from function import *
import numpy as np

class C_Killer():

	def __init__(self):

		self.redis_ip = config.redis_ip 
		self.redis_port = config.redis_port
		self.r = redis.StrictRedis(host=self.redis_ip, port=self.redis_port, decode_responses=True)
		self.thread_watch_span = config.thread_watch_span
		
		# store the information of all dynamic sessions
		self.dynamic_sess_groups = []
		# store the information of all static sessions
		self.static_sess_groups = []
		# store the threads
		self.thread_array = []
		# store the real session for result estimation
		self.real_session = {}
		# At the initialization, we will load all requests
		self.all_requests = queue.Queue(10000)
		# request pointer
		self.request_pointer = 0
		# the request recognized as crawler
		self.crawler = []
		# the request recognized as malicious crawler
		self.malicious_crawler = []


	def thread_load_new_request(self):
		'''
		This thread is used to load new request to the queue

		'''
		while True:
			
			now_request_count = int(self.r.get('request_count'))
			if now_request_count > self.request_pointer:
				request = self.r.hget('my_request',self.request_pointer + 1)
				if request:
					request = json.loads(request)
					request['id'] = self.request_pointer + 1
					self.all_requests.put(request)
				else:
					log.error('fail to load: %s' %str(self.request_pointer + 1))

				self.request_pointer += 1

			else:
				# refresh every 3 seconds
				time.sleep(5)
				log.warning('nothing new...')


	def thread_handle_new_request(self):
		'''
		This thread is used to handle with request
		'''
		while True:
			if not self.all_requests.empty():
				
				request = self.all_requests.get()

				# crawler judgement and session classification
				if self.static_malicious_detection(request):
					request['crawler'] = 'malicious-crawler'
					self.malicious_crawler.append(request['id'])
				elif request['crawler']=='normal' and self.static_crawler_detection(request):
					request['crawler'] = 'crawler'
					self.crawler.append(request['id'])
				elif request['dynamic_info'] and self.dynamic_crawler_detection(request):
					request['crawler'] = 'crawler'
					self.crawler.append(request['id'])

				if request['real_session']:
					if request['real_session'] in self.real_session.keys():
						self.real_session[request['real_session']].append(request['id'])
					else:
						self.real_session[request['real_session']] = [request['id']]
				else:
					# set the real_session to test_session if the real_session is not set
					log.error('no real session')
					if 'test_session' in self.real_session.keys():
						self.real_session['test_session'].append(request['id'])
					else:
						self.real_session[request['real_session']] = [request['id']]
						
				self.session_classify(request)

			else:
				log.warning('empty queue detected...')
				time.sleep(5)
				


	def thread_crawler_detection(self):
		while True:
			time.sleep(config.crawler_detection_span)
			log.info('start crawler detection...')
			self.crawler_detection()


	def crawler_detection(self):
		'''
		This thread will mark the session_group as crawler,normal or malicious_crawler

		'''
		all_session_groups = self.static_sess_groups + self.dynamic_sess_groups
		for session_id in range(len(all_session_groups)):

			session_group = all_session_groups[session_id]

			# malicious-crawler is already the final version, so skip all of the tests
			if session_group['crawler'] == 'malicious-crawler':
				continue

			# detect by malicious request
			if self.detect_by_malicious_request(session_group,session_id):
				continue

			# detect crawler type by session_info
			session_info = 	self.extract_session_info(session_group)
			# log.context(str(session_info))
			crawler_info = self.judge_session_info(session_info)
			if crawler_info == 'malicious-crawler':
				self.handle_malicious(session_group,session_id)
				continue
			elif crawler_info == 'crawler':
				self.handle_crawler(session_group,session_id)
				continue

			# for cralwer sessions, nothing to do
			if session_group['crawler'] == 'normal':
				self.detect_by_crawler_request(session_group,session_id)
				continue


	def handle_malicious(self,session_group,session_id):
		self.update_by_sess_id(session_id,'malicious-crawler')
		# add the crawler's ip to black_list
		src_ip_dict = {}
		for request_id in session_group['requests']:
			request = json.loads(self.r.hget('my_request',request_id))

			# remove the 172.16.10.1 it's the public IP address
			if not (request['src_ip'] == '172.16.10.1'):
				if 'User-Agent' in request['headers'].keys():
					src_ip_dict[request['src_ip']] = {"webdriver":"phantomjs" if 'PhantomJS' in request['headers']['User-Agent'] else "unknown"}
				else:
					src_ip_dict[request['src_ip']] = {"webdriver":"unknown"}

		insert_black_ip_dict(self.r,src_ip_dict)


	def handle_crawler(self,session_group,session_id):
		self.update_by_sess_id(session_id,'crawler')


	def detect_by_malicious_request(self,session_group,session_id):
		# for the crawler sessions and normal sessions, try to detect wether they are malicious
		for request_id in session_group['requests']:
			if request_id in self.malicious_crawler:
				self.handle_malicious(session_group,session_id)
				return True
		return False

	def detect_by_crawler_request(self,session_group,session_id):
		# for normal sessions, try to detect wether they are crawler
		for request_id in session_group['requests']:
			if request_id in self.crawler:
				self.handle_crawler(session_group,session_id)
				# self.handle_malicious(session_group,session_id)
				return True
		return False

	def update_by_sess_id(self,session_id,type_value):
		'''
		This method is used to update the session crawler type by session_id
		'''
		offset = session_id - len(self.static_sess_groups)
		if offset >= 0:
			self.dynamic_sess_groups[offset]['crawler'] = type_value
		else:
			self.static_sess_groups[session_id]['crawler'] = type_value


	def extract_session_info(self,session_group):
		'''
		session features extraction
		'''
		total_count = len(session_group['requests'])
		_4xx_5xx_code_count = 0
		page_count = 0
		time_seq = []

		for request_id in session_group['requests']:
			request = json.loads(self.r.hget('my_request',request_id))
			# record the 4xx/5xx code
			if int(request['response_code'])>=400:
				_4xx_5xx_code_count += 1
			# record the page count
			for postfix in config.page_postfix:
				if postfix in request['path']:
					page_count += 1
					break
			# record the time sequence, set the accuracy to int
			time_seq.append(int(request['request_time']))

		time_seq.sort()
		max_request_frequency = self.get_max_freq(time_seq)
		std_deviation = np.std(time_seq)

		# log.success('######### session_info #########')
		# log.success('total_count => %d' %total_count)
		# log.success('_4xx_5xx_code_count => %d' %_4xx_5xx_code_count)
		# log.success('_4xx_5xx_code_count => %f' %(float(_4xx_5xx_code_count)/total_count))
		# log.success('page_count => %d' %page_count)
		# log.success('page_ratio => %f' %(float(page_count)/total_count))
		# log.success('max_request_frequency => %d' %max_request_frequency)
		# log.success('std_deviation => %f' %std_deviation)
		# log.success('######### session_info #########')

		return [total_count,_4xx_5xx_code_count,(float(_4xx_5xx_code_count)/total_count),page_count,(float(page_count)/total_count),max_request_frequency,std_deviation]

	def judge_session_info(self,session_info):

		total_count = session_info[0]
		_4xx_5xx_code_count = session_info[1]
		_4xx_5xx_code_ratio = session_info[2]
		page_count = session_info[3]
		page_ratio = session_info[4]
		max_request_frequency = session_info[5]
		std_deviation = session_info[6]

		if total_count > 20:
			if max_request_frequency > 100:
				return 'malicious-crawler'
			if _4xx_5xx_code_ratio >= 0.2 or page_ratio >= 0.35 or max_request_frequency > 50 and std_deviation<10:
				return  'crawler'
		# insuffcient count to using our model
		return 'normal'


	def get_max_freq(self,time_seq):
		'''
		get max_freq from the specific time_seq
		'''
		max_freq  = 1
		for i in range(len(time_seq)):
			tmp_max_freq = 1
			# if this conditions is met, then there be bigger max_freq since the left number is insufficient to update the max_freq
			if i + max_freq >= len(time_seq):
				break
			for j in range(len(time_seq) - i - 1):
				if time_seq[i+j] - time_seq[i] <= config.max_freq_time:
					tmp_max_freq += 1
					# next one
					continue
				else:
					break
			# update max_freq
			if max_freq < tmp_max_freq:
				max_freq = tmp_max_freq
			break
		return max_freq


	def session_classify(self,request):
		fp = calc_fp(request)
		new_session = 1

		if request['js_fp']!='unknown':
			my_sess_group = self.dynamic_sess_groups
			my_weight = config.dynamic_weight
			my_session_type = 'dynamic'
		else:
			my_sess_group = self.static_sess_groups
			my_weight = config.static_weight
			my_session_type = 'static'

		for session_group in my_sess_group:
			sim_score = calc_fp_similarity(session_group['fp'],fp,my_weight)
			# the last element of config is the threshold score
			if sim_score >= my_weight[-1]:
				# log.context(str(sim_score))
				session_group['requests'].append(request['id'])
				if request['src_ip'] not in session_group['ips']:
					session_group['ips'].append(request['src_ip'])
				new_session = 0
				break

		# no match session, create new one
		if new_session:
			log.info('new session created!')
			my_sess_group.append({'fp':fp, 'ips':[request['src_ip']], 'requests':[request['id']], 'type':my_session_type,'crawler':'normal'})

			
	def calc_accuracy(self):
		dynamic_session_count = len(self.dynamic_sess_groups)
		static_session_count = len(self.static_sess_groups)
		dynamic_count = sum([len(x['requests']) for x in self.dynamic_sess_groups])
		static_count = sum([len(x['requests']) for x in self.static_sess_groups])
		static_session_penalty = static_session_count - len(self.real_session)

		dynamic_match_count = 0
		static_match_count = 0

		for session_group in (self.dynamic_sess_groups + self.static_sess_groups):
			tmp = { x:0 for x in  self.real_session}
			# get the session distribution in session_group
			for request_id in session_group['requests']:
				for tmp_session in self.real_session:
					if request_id in self.real_session[tmp_session]:
						tmp[tmp_session] += 1
						break

			if session_group['type'] == 'dynamic':
				dynamic_match_count += max(tmp.values())
			else:
				static_match_count += max(tmp.values())
			
		static_accuracy = float(static_match_count - static_session_penalty)/static_count 
		dynamic_accuracy = float(dynamic_match_count)/dynamic_count

		log.success('dynamic_session_count => %d' %dynamic_session_count)
		log.success('static_session_count => %d' %static_session_count)
		log.success('dynamic_count => %d' %dynamic_count)
		log.success('static_count => %d' %static_count)
		log.success('dynamic_match_count => %d' %dynamic_match_count)
		log.success('static_match_count => %d' %static_match_count)
		log.success('static_session_penalty => %d' %static_session_penalty)
		log.success('static_accuracy => %f' %static_accuracy)
		log.success('dynamic_accuracy => %f' %dynamic_accuracy)
		log.success('black_ip list => ' + str(get_black_ip(self.r)))

		return {"dynamic_session_count":dynamic_session_count,"static_session_count":static_session_count,"dynamic_count":dynamic_count,"static_count":static_count,"dynamic_match_count":dynamic_match_count,"static_match_count":static_match_count,"static_session_penalty":static_session_penalty,"static_accuracy":static_accuracy,"dynamic_accuracy":dynamic_accuracy}


	def static_crawler_detection(self,request):
		'''
		detect crawler by static features

		'''
		# by http method
		if request['method'] not in config.allow_method:
			return True
		# by url path
		if request['path'] in config.crawler_path:
			return True
		# by user agent
		if not 'User-Agent' in request['headers'].keys():
			return True
		for crawler_string in config.crawler_strings:
			if crawler_string in request['headers']['User-Agent']:
				return True
		# by http header count
		if len(request['headers']) <= config.crawler_headers_count:
			return True

		return False


	def static_malicious_detection(self,request):
		'''
		detect malicious crawler by malicious payload
		'''
		# only run with import field
		raw_data = request['path'] + "\n" + request['content'] + "\n" + (request['cookies'] if 'cookies' in request.keys() else '')
		# print(request)
		for rule in config.malicious_rules:
			tmp = re.findall(rule,raw_data)
			if tmp and tmp[0]:
				log.error('malicious detected: %s' %str(tmp))
				# log.error(rule)
				return True
		return False
	

	def dynamic_crawler_detection(self,request):
		'''
		detect crawler by dynamic features. return with the type of the crawler
		'''
		dynamic_info = request['dynamic_info']
		dynamic_info = {x.split(': ')[0]:x.split(': ')[1] for x in dynamic_info.split('\n') if x}
		score = 0

		# judge the crawler by window size
		window_size = int(dynamic_info['window size'].split('x')[0]) * int(dynamic_info['window size'].split('x')[1])
		if window_size < config.window_size_threshold:
			log.error('possible crawler detected by window size: %d' %window_size)
			score += 2

		# judge the crawler by close time
		closed_time = int(dynamic_info['closed time(ms)'])
		if closed_time < config.closed_time_threshold:
			log.error('possible crawler detected by closed time: %d' %closed_time)
			score += 2

		# judge the crawler by special function
		if dynamic_info['special functions']!='None':
			log.error('possible crawler detected by special functions: %s' %dynamic_info['special functions'])
			score += 2

		# judge the crawler by mouse movement
		mouse_actions = sum(json.loads(dynamic_info['mouse and keyboard events']).values())
		if mouse_actions == 0:
			log.error('possible crawler detected by mouse_actions')
			score += 1

		# judge the crawler by plugins
		if not dynamic_info['plugins']:
			log.error('possible crawler detected by plugins')
			score += 1

		if score >= config.dynamic_score:
			return True
		return False


	def thread_demo(self):
		'''
		this thread is used for demon purpose, it will output the session info, and store the sessions into the file for web page demonstration
		'''
		while True:
			time.sleep(10)
			self.calc_accuracy()
			res = {}
			res['dynamic_sess_groups'] = self.dynamic_sess_groups
			res['static_sess_groups'] = self.static_sess_groups
			# link it to demo docker
			open('/root/res/res.json','w').write(json.dumps(res))
			
		



	def thread_watch(self):
		'''
			start a thread to print the status of all alive status
		'''
		while True:

			time.sleep(self.thread_watch_span)

			res = ''
			res += '######  thread status ######\n'
			for my_thread in self.thread_array:
				if my_thread.isAlive():
					res +=  my_thread.name + ' => ' + 'Alive\n' if my_thread.isAlive() else 'Dead\n'
			res += '######  status ends  ######\n'
			
			# log the requests queue size
			if not self.all_requests.empty():
				all_requests_queue_count = str(len(list(self.all_requests.queue)))
			else:
				all_requests_queue_count = 'no'
			res += "[+] existing all_requests queue: %s\n"%all_requests_queue_count

			log.context(res)


	def banner(self):
		my_banner = ""
		my_banner += "  ____      _    _ _ _           \n"
		my_banner += " / ___|    | | _(_) | | ___ _ __ \n"
		my_banner += "| |   _____| |/ / | | |/ _ \ '__|\n"
		my_banner += "| |__|_____|   <| | | |  __/ |   \n"
		my_banner += " \____|    |_|\_\_|_|_|\___|_|   \n"
		my_banner += "                                 \n"
		my_banner += "                 version 0.1     \n"
		my_banner += "\n"
		print(my_banner)


if __name__ == '__main__':
	
	c = C_Killer()
	c.banner()

	t = threading.Thread(target=c.thread_watch,name='watch_dog')
	log.info('one thread for watchdog has been created!')
	c.thread_array.append(t)

	t = threading.Thread(target=c.thread_load_new_request,name='load_new_request')
	log.info('one thread for load_new_request has been created!')
	c.thread_array.append(t)

	for i in range(config.handler_thread_num):
		t = threading.Thread(target=c.thread_handle_new_request,name='handler_%d'%(i+1))	
		log.info('one thread for handler has been created!')
		c.thread_array.append(t)

	t = threading.Thread(target=c.thread_crawler_detection,name='crawler_detection')
	log.info('one thread for crawler_detection has been created!')
	c.thread_array.append(t)

	t = threading.Thread(target=c.thread_demo,name='crawler_demo')
	log.info('one thread for crawler_demo has been created!')
	c.thread_array.append(t)

	for t in c.thread_array:
		t.setDaemon(True)
		t.start()

	try:
		while 1:
			# print 'main thread hello'
			time.sleep(5)
			# print 'main thread alive'
	except KeyboardInterrupt:
		log.error("Program stoped by user, existing...")
		c.crawler_detection()
		log.context("dynamic_session_group" + str(c.dynamic_sess_groups))
		log.info("static_session_group" + str(c.static_sess_groups))
		log.context("real_session =>" + str(c.real_session))
		c.calc_accuracy()
		sys.exit()
