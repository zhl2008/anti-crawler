#!/usr/bin/env python3

import os
import sys
import threading
import queue
import time
import log
import json
import config
import redis

class C_Killer():

	def __init__(self):

		self.redis_ip = config.redis_ip 
		self.redis_port = config.redis_port
		self.r = redis.StrictRedis(host=self.redis_ip, port=self.redis_port, decode_responses=True)
		self.thread_watch_span = config.thread_watch_span
		self.fp_queue = queue.Queue(1000)
		self.sess_queue = queue.Queue(1000)
		self.thread_array = []

		# init 
		self.all_session_fp = self.load_session_fp()
		self.all_session_request = self.load_session_request()


	def load_session_fp(self):
		'''
			load the session,fingerprint information from database
		'''
		tmp = {}
		session_count = self.r.get('session_count')
		if not session_count:
			session_count = 0
		else:
			session_count = int(session_count)
		for i in range(1,session_count+1):
			tmp[str(i)] = json.loads(self.r.hget('all_session_fp',str(i)))

		print(tmp)
		return tmp


	def load_session_request(self):

		tmp = {}
		session_count = self.r.get('session_count')
		if not session_count:
			session_count = 0
		else:
			session_count = int(session_count)
		for i in range(1,session_count+1):
			tmp[str(i)] = json.loads(self.r.hget('all_session_request',str(i)))

		print(tmp)
		return tmp


	def fp_queue_thread(self):

		fp = self.r.get('fp_pointer')
		if fp:
			_id = str(int(fp)+1)
		else:
			_id = 1

		while True:

			# waitting for next request
			request_data = self.r.hget('my_request',_id)
			if not request_data:
				time.sleep(3)
				continue

			# if the queue is not full and _id has not reached to the boundary
			if  not self.fp_queue.full():
				log.context('put request into fp_queue: %s'%request_data)
				self.fp_queue.put((str(_id),request_data))
				_id = str(int(_id) + 1)
				self.r.set('fp_pointer',str(_id))

			elif int(_id) <= int(self.set('request_count')):
				time.sleep(3)
				log.warning('waiting for more requests...')

			else:
				time.sleep(3)
				log.warning('full fingerprint queue detected!')


	def sess_queue_thread(self):

		sess = self.r.get('sess_pointer')
		if sess:
			_id = str(int(sess)+1)
		else:
			_id = 1

		while True:

			# waitting for next request
			fingerprint = self.r.hget('my_fingerprint',str(_id))
			if not fingerprint:
				time.sleep(3)
				continue

			if  not self.sess_queue.full():
				log.context('put request into sess_queue: %s'%fingerprint)
				self.sess_queue.put((_id,fingerprint))
				_id = str(int(_id) + 1)
				self.r.set('sess_pointer',str(_id))

			elif int(_id) <= int(self.set('request_count')):
				time.sleep(3)
				log.warning('waiting for more requests...')

			else:
				time.sleep(3)
				log.warning('full session queue detected!')



	def gen_fp_thread(self):

		while True:
			if not self.fp_queue.empty():
				_id,request_data = self.fp_queue.get()
				fingerprint = self.gen_fp(request_data)
				self.r.hset('my_fingerprint',_id,json.dumps(fingerprint))
			else:
				time.sleep(3)
				log.warning('empty fingerprint queue detected!')


	def gen_sess_thread(self):

		while True:
			if not self.sess_queue.empty():
				_id,fp_1 = self.sess_queue.get()
				fp_1 = json.loads(fp_1)
				
				match_flag = 0
				# to quicken the session classification, we make a mirorr of the database in the memory when the class is initialized
				for session_id in self.all_session_fp:
					fp_2 = self.all_session_fp[session_id]

					# if the fp_1 matches with fingerprint in already-exists session, then label add request to the session, and combine the fp_1 fp_2 together
					if self.compare_fp(fp_1,fp_2):
						self.all_session_request[session_id].append(str(_id))
						self.all_session_fp[session_id] = self.combine_fp(fp_1,fp_2)

						self.r.hset('all_session_request',session_id,json.dumps(self.all_session_request[session_id]))
						self.r.hset('all_session_fp',session_id,json.dumps(self.all_session_fp[session_id]))

						match_flag = 1

						break
				# if none of the fingerprint matches the request fingerprint, then generate a new session
				if not match_flag:
					session_id = self.r.get('session_count')
					if session_id:
						session_id = str(int(session_id) + 1)
					else:
						session_id = 1
					self.r.set('session_count',str(session_id))

					self.all_session_request[session_id] = [str(_id)]
					self.all_session_fp[session_id] = fp_1
					self.r.hset('all_session_request',session_id,json.dumps(self.all_session_request[session_id]))
					self.r.hset('all_session_fp',session_id,json.dumps(self.all_session_fp[session_id]))
					log.info('new session add!')
				
			else:
				time.sleep(3)
				log.warning('empty session queue detected!')



	def gen_fp(self,request_data):
		request = json.loads(request_data)

		# for test
		return [['00112233'],['44556677'],['8899aabb']]


	def compare_fp(self,fp_1,fp_2):
		print(fp_1)
		print(fp_2)
		print(self.all_session_fp)
		'''
		return True when fp_1 is equivalent to fp_2

		'''

		return True

	def combine_fp(self,fp_1,fp_2):
		'''
		combine two fingerprint together:
		for example:
			fp_1 = [['00112233'],['44556677'],['8899aabb']]
			fp_2 = [['00112233','eeffeeff'],[''],['8899aaaa']]
		then:
			fp_1 + fp_2 
			= [['00112233','eeffeeff'],['44556677'],['8899aabb','8899aaaa']]

		'''	
		assert('list' in str(type(fp_1)))
		assert('list' in str(type(fp_2)))
		res = []
		tmp = [x for x in [list(set(fp_1[0] + fp_2[0])),list(set(fp_1[1] + fp_2[1])),list(set(fp_1[2] + fp_2[2]))] ]
		for t in tmp:
			if '' in t:
				t.remove('')
			res.append(t)
		return res



	def judge_robot(self):
		'''
		This function is utilized to judge whether a request is from a robot by using some very simple rules What should be stresssed is that the rules here are extracted from other traditional anti-crawler methods, which is not the innovation of this paper. 

		And the crawler detection based on traditional rules is called online-analysis, it would help us to label some sessions which might contain crawler request, and help us obtain a better understanding of the crawler behaviors at the session granularity.

		Finally, we are abled to summarize the 
		'''

		pass


	def session_classify(self):
		pass


	def redis_clean(self):
		self.r.flushdb()

	def redis_init(self):
		'''
			The pointer is used to label the sequence of the requests that has been disposed, IN CASE that the program goes crash, you won't need to start over.
		'''
		self.r.set('fp_pointer','0')
		self.set('sess_pointer','0')
		self.set('request_count','0')
		self.set('session_count','0')


	def thread_watch(self):
		'''
			start a thread to print the status of all alive status
		'''
		while True:

			time.sleep(self.thread_watch_span)

			res = ''
			res += '######  thread status ######\n'
			for my_thread in self.thread_array:
				if my_thread.isAlive():
					res +=  my_thread.name + ' => ' + 'Alive\n' if my_thread.isAlive() else 'Dead\n'
			res += '######  status ends  ######\n'

			if not self.fp_queue.empty():
				# print self.exploit_queue.qsize()
				fp_queue_count = str(len(list(self.fp_queue.queue)))
			else:
				fp_queue_count = 'no'
			res += "[+] existing fp queue: %s\n"%fp_queue_count

			if not self.sess_queue.empty():
				# print self.exploit_queue.qsize()
				sess_queue_count = str(len(list(self.sess_queue.queue)))
			else:
				sess_queue_count = 'no'
			res += '[+] existing session queue: %s'%sess_queue_count

			log.context(res)


	def banner(self):
		my_banner = ""
		my_banner += "  ____      _    _ _ _           \n"
		my_banner += " / ___|    | | _(_) | | ___ _ __ \n"
		my_banner += "| |   _____| |/ / | | |/ _ \ '__|\n"
		my_banner += "| |__|_____|   <| | | |  __/ |   \n"
		my_banner += " \____|    |_|\_\_|_|_|\___|_|   \n"
		my_banner += "                                 \n"
		my_banner += "\n"
		print(my_banner)
		

if __name__ == '__main__':
	
	c = C_Killer()

	c.banner()

	t = threading.Thread(target=c.thread_watch,name='watch_dog')
	log.info('one thread for watchdog has been created!')
	c.thread_array.append(t)


	for i in range(config.gen_fp_thread_num):
		t = threading.Thread(target=c.gen_fp_thread,name='gen_fp_thread_%d'%(i+1))	
		log.info('one thread for gen_fp has been created!')
		c.thread_array.append(t)

	for i in range(config.gen_sess_thread_num):
		t = threading.Thread(target=c.gen_sess_thread,name='gen_sess_thread_%d'%(i+1))	
		log.info('one thread for gen_sess has been created!')
		c.thread_array.append(t)

	t = threading.Thread(target=c.fp_queue_thread,name='fp_queue_thread')
	log.info('one thread for fp_queue_thread has been created!')
	c.thread_array.append(t)

	t = threading.Thread(target=c.sess_queue_thread,name='sess_queue_thread')
	log.info('one thread for sess_queue_thread has been created!')
	c.thread_array.append(t)

	for t in c.thread_array:
		t.setDaemon(True)
		t.start()

	try:
		while 1:
			# print 'main thread hello'
			time.sleep(5)
			# print 'main thread alive'
	except KeyboardInterrupt:
		log.error("Program stoped by user, existing...")
		sys.exit()