#!/usr/bin/env python3
import hashlib
import config
import json

def md5(string):
	return hashlib.md5(string.encode('utf-8'))

def calc_fp(request_data):

	fp_group_penalty = ['Connection','Accept','Content-Type']

	fp_1 = md5(request_data['User-Agent']).hexdigest() if ('User-Agent' in request_data.keys() and  request_data['User-Agent']) else '0'*32
	fp_2 = md5(''.join(request_data['headers'].keys())).hexdigest()
	fp_3 = md5(request_data['path'][:20]).hexdigest() if request_data['path'] else '0'*32

	
	fp_penalty = [ md5(request_data['headers'][x]).hexdigest() for x in fp_group_penalty if x in request_data['headers'].keys()]
	fp_group = [ md5(request_data['headers'][x]).hexdigest() for x in request_data['headers'].keys() if (len(x) > 0 and x not in fp_group_penalty)]

	fp_js = request_data['js_fp']

	return [fp_1, fp_2, fp_3, fp_group,fp_penalty,fp_js]

def calc_fp_similarity(fp_1,fp_2,weight):
	'''
	This function is utilized to calc the similarity between 2 fingerprints

	'''
	score = 0
	if fp_1[0] == fp_2[0] and fp_1[0] != '00000000000000000000000000000000':
		score += weight[0]

	if fp_1[1] == fp_2[1] and fp_1[1] != '00000000000000000000000000000000':
		score += weight[1]

	if fp_1[2] == fp_2[2] and fp_1[2] != '00000000000000000000000000000000':
		score += weight[2]

	# fp_group for header fp
	for i in range(len(fp_1[3])):
		if fp_1[3][i] in fp_2[3]:
			score += weight[3]

	# for fp_penalty
	for i in range(len(fp_1[4])):
		if fp_1[4][i] in fp_2[4]:
			score += weight[4]
	# with dynamic fingerprint
	if len(fp_1) == 6:
		if fp_1[5] == fp_2[5]:
			score += weight[5]
	return score/float( max(len(fp_1[3])+len(fp_1[4]) , len(fp_2[3])+len(fp_1[4]))   + 2 )


def get_black_ip(r):
    black_ips = r.get('black_ips')
    if not black_ips:
        return {}
    return json.loads(black_ips)

def insert_black_ip(r,black_ip,black_info):
    black_ips = get_black_ip(r)
    black_ips[black_ip] = black_info
    r.set('black_ips',json.dumps(black_ips))
    return black_ips

def insert_black_ip_dict(r,black_ip_dict):
    black_ips = get_black_ip(r)
    for black_ip in black_ip_dict:
    	black_ips[black_ip] = black_ip_dict[black_ip]
    r.set('black_ips',json.dumps(black_ips))
    return black_ips

if __name__ == '__main__':



	request_data = {"request_time": 1575432377.1839228, "src_ip": "172.16.10.1", "dst_ip": "172.16.10.1", "path": "/my_info.php?info=aW50ZXJuYWwgaXA6IDE3Mi4xNi4xMC4zOwpmaW5nZXJwcmludDogMWE4MmNjNTdhOTdkN2E5MzFhODJjYzU3MjBkZTk5NzcKd2luZG93IHNpemU6IDEyMDB4NjAwCmNsb3NlZCB0aW1lKG1zKTogMTc5CmJhdHRlcnkgc3RhdHVzOiAxMDAlOzAgU2Vjb25kcztJbmZpbml0eSBTZWNvbmRzO0NoYXJnaW5nOwp3ZWJhdWRpbyBzdGF0dXM6IHN1cHBvcnRlZAp3ZWJzb2NrZXQgc3RhdHVzOiBzdXBwb3J0ZWQKd2ViR0wgc3RhdHVzOiBzdXBwb3J0ZWQKd2VicnRjIHN0YXR1czogc3VwcG9ydGVkCnBsdWdpbnM6IApzcGVjaWFsIGZ1bmN0aW9uczogTm9uZQptb3VzZSBhbmQga2V5Ym9hcmQgZXZlbnRzOiB7Im9ubW91c2VvdmVyIjowLCJvbm1vdXNlb3V0IjowLCJvbm1vdXNlZG93biI6MCwib25tb3VzZXVwIjowLCJvbm1vdXNlbGVhdmUiOjAsIm9ubW91c2VlbnRlciI6MCwib25kYmxjbGljayI6MCwib25jb250ZXh0bWVudSI6MCwib25jbGljayI6MCwib25rZXlkb3duIjowLCJvbmtleXByZXNzIjowLCJvbmtleXVwIjowfQo=", "content": "", "cookie": {"real_session": "69214877", "js_fp": "1a82cc57a97d7a931a82cc5720de9977"}, "headers": {"Host": "172.16.10.1", "Connection": "keep-alive", "Upgrade-Insecure-Requests": "1", "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) HeadlessChrome/70.0.3509.0 Safari/537.36", "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8", "Referer": "http://172.16.10.1:8001/get_dynamic.html", "Accept-Encoding": "gzip, deflate"}, "response_code": 404, "response_len": 285, "real_session": "69214877", "js_fp": "1a82cc57a97d7a931a82cc5720de9977", "method": "GET", "crawler": "normal", "black": "", "query_dict": {"info": "aW50ZXJuYWwgaXA6IDE3Mi4xNi4xMC4zOwpmaW5nZXJwcmludDogMWE4MmNjNTdhOTdkN2E5MzFhODJjYzU3MjBkZTk5NzcKd2luZG93IHNpemU6IDEyMDB4NjAwCmNsb3NlZCB0aW1lKG1zKTogMTc5CmJhdHRlcnkgc3RhdHVzOiAxMDAlOzAgU2Vjb25kcztJbmZpbml0eSBTZWNvbmRzO0NoYXJnaW5nOwp3ZWJhdWRpbyBzdGF0dXM6IHN1cHBvcnRlZAp3ZWJzb2NrZXQgc3RhdHVzOiBzdXBwb3J0ZWQKd2ViR0wgc3RhdHVzOiBzdXBwb3J0ZWQKd2VicnRjIHN0YXR1czogc3VwcG9ydGVkCnBsdWdpbnM6IApzcGVjaWFsIGZ1bmN0aW9uczogTm9uZQptb3VzZSBhbmQga2V5Ym9hcmQgZXZlbnRzOiB7Im9ubW91c2VvdmVyIjowLCJvbm1vdXNlb3V0IjowLCJvbm1vdXNlZG93biI6MCwib25tb3VzZXVwIjowLCJvbm1vdXNlbGVhdmUiOjAsIm9ubW91c2VlbnRlciI6MCwib25kYmxjbGljayI6MCwib25jb250ZXh0bWVudSI6MCwib25jbGljayI6MCwib25rZXlkb3duIjowLCJvbmtleXByZXNzIjowLCJvbmtleXVwIjowfQo="}, "dynamic_info": "internal ip: 172.16.10.3;fingerprint: 1a82cc57a97d7a931a82cc5720de9977window size: 1200x600closed time(ms): 179battery status: 100%;0 Seconds;Infinity Seconds;Charging;webaudio status: supportedwebsocket status: supportedwebGL status: supportedwebrtc status: supportedplugins: special functions: Nonemouse and keyboard events: {\"onmouseover\":0,\"onmouseout\":0,\"onmousedown\":0,\"onmouseup\":0,\"onmouseleave\":0,\"onmouseenter\":0,\"ondblclick\":0,\"oncontextmenu\":0,\"onclick\":0,\"onkeydown\":0,\"onkeypress\":0,\"onkeyup\":0}"}

	request_data_2 = {"request_time": 1575432376.2993517, "src_ip": "172.16.10.1", "dst_ip": "172.16.10.1", "path": "/taint_link", "content": "", "cookie": {"real_session": "69214877", "js_fp": "1a82cc57a97d7a931a82cc5720de9977"}, "headers": {"Host": "172.16.10.1", "Connection": "keep-alive", "Upgrade-Insecure-Requests": "1", "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) HeadlessChrome/70.0.3509.0 Safari/537.36", "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8", "Referer": "http://172.16.10.1:8001/plus/qrcode.php?id=0&type=index", "Accept-Encoding": "gzip, deflate"}, "response_code": 404, "response_len": 284, "real_session": "69214877", "js_fp": "1a82cc57a97d7a931a82cc5720de9977", "method": "GET", "crawler": "crawler", "black": "", "query_dict": {}, "dynamic_info": ""}


	# print(calc_fp(request_data))
	# print(calc_fp(request_data_2))
	print(calc_fp_similarity(calc_fp(request_data),calc_fp(request_data),config.dynamicexport__weight))