#!/usr/bin/env python2


import redis
import json



client =redis.Redis(host='127.0.0.1',port=6379,db=0)


def export_redis():
	data_set = client.hgetall('my_request')
	data_count = client.get('request_count')
	open('redis.json','w').write(json.dumps(data_set))


def clean_redis():
	client.flushall()



def import_redis():
	data_set = json.load(open('redis.json'))
	data_count = len(data_set)
	_id = 1
	for data_key in data_set:
		client.hset('my_request',_id,data_set[data_key])
		_id += 1
	client.set('request_count',data_count)



if __name__ == '__main__':
	export_redis()
	clean_redis()
	# import_redis()


