#!/usr/bin/env python2

import threading
import Queue
from robot import robot
import time
from function import *
import random
import sys
import urlparse
from selenium.webdriver.common.alert import Alert
import log


'''
This program is utilized to create requests from web-crawlers under diiferent crawling policies,
the policies include the following entries:

1. crawling target (specific type info [such as img], specific page, the structure of the all website, vulnerable scanner, dir brute force)

2. crawling speed  (slow, medium, quick, very quick, random)

3. crawling iteration algorithm (BFS, DFS)

4. different webdriver (python, curl, firefox, chrome)

5. adversarial policies ( ip obfuscation, header mutation)


How to initialize the crawler:

for every crawler in our program, they are capable to send a sequence of requests in a time slot. And our research is based on the requests in this time slot. For every crawler in the time slot, we may provide the following parameter to initialize it.

1. website's ip and port
2. policies ( listed above)
3. limit length (limit the length of the request)


The data structure of the request:

{"request_time": 1561956984.3283665, "src_ip": "172.16.10.1", "dst_ip": "172.16.10.1", "path": "/plus/qrcode.php?action=get_qrcode&type=index&id=0", "content": "", "cookie": {}, "headers": {"Host": "172.16.10.1", "Connection": "keep-alive", "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) HeadlessChrome/70.0.3509.0 Safari/537.36", "Accept": "image/webp,image/apng,image/*,*/*;q=0.8", "Referer": "http://172.16.10.1:8001/plus/qrcode.php?id=0&type=index", "Accept-Encoding": "gzip, deflate"}, "response_code": 200, "response_len": 420}

The data structure of the response:
{"response_code": 200, "response_len": 6, "content":"aaaaaaa"}

'''

class crawler():


	def __init__(self, url, limit_len, policies):
		self.limit_len = limit_len
		self.p_target = policies[0]
		self.p_speed = policies[1]
		self.p_algo = policies[2]
		self.p_driver = policies[3]
		self.p_adv = policies[4]

		self.visit_queue = Queue.Queue(1000)
		self.base_url = url
		self.done_url = []
		# real session should be uniq
		self.real_session =  random.randint(0,99999999)


	def _get_all_queue(self,queue):
	    result_list = []
	    while not queue.empty():
	        result_list.append(queue.get())
	    return result_list

	def _load_malicious_requests(self,filename):

		return [{"path":"/index%d.php?cmd=system('ls')%d"%(i,i)} for i in range(100)]

	def _load_brute_requests(self,filename):
		brute_dirs = [{"path":'/'+x.strip()} for x in open('./brute.txt').readlines()]
		return brute_dirs[:1000]
		# return [{"path":"/index.php"},{"path":"/index.php.bak"},{"path":"/index.php~"}]

	def _parse_links(self,response,request):
		# empty response may be produced due to some unknown internal errors
		if not response:
			return []
		if not response['content']:
			return []
		links = get_all_links(response['content'],'http://172.16.10.1:8001',request['path'])
		log.success('find new links %s' %(str(links)))
		return links


	def _init_visit_queue(self):
		'''
		decide which data type the crawler may focus on
		'''

		# # image type
		# if self.p_target == 0:
		# 	self.visit_queue = self.image_queue
		# # fixed page (random choice from all page_queue)
		# elif self.p_target == 1:
		# 	fix_page = random.choice(self._get_all_queue(self.page_queue))
		# 	for i in range(1000):
		# 		self.visit_queue.put(fix_page)
		# # all structure 
		# elif self.p_target == 2:
		# 	self.visit_queue = self.all_queue

		if self.p_target in [0,1,2]:
			self.visit_queue.put({"path":"/"})
		# vulnerable scanner
		elif self.p_target == 3:
			for malicous_request in self._load_malicious_requests('malicious.json'):
				self.visit_queue.put(malicous_request)
		elif self.p_target == 4:
			for brute_request in self._load_brute_requests('brute.txt'):
				self.visit_queue.put(brute_request)

	def _link_type(self,link):

		# two types 'img' ,'page' and 'other'
		if '.png' in link or '.jpg' in link or '.gif' in link:
			return 'img'
		elif '.css' in link or '.js' in link or '.font' in link:
			return 'other'
		else:
			return 'page'

	def crawling_sleep(self):
		'''
		the speed policies is labeled from 0-4 to map itself to the time in the time_sleep_array
		'''
		time_sleep_array = [30, 20 , 5 , 1, random.randint(0,30)]
		time.sleep(time_sleep_array[self.p_speed])	

	def crawling_algorithm(self,response,request):
		'''
		parse the http response into the links, and arrange them by the BFS or DFS

		'''
		# do not apply the crawling algorithm if the target is malicious payload or dir brute
		if self.p_target in [0,1,2]:
			links = self._parse_links(response,request)
			tmp_queue = Queue.Queue(1000)
			for link in links:
				# avoid redudancy
				if {"path":link} in list(self.visit_queue.queue):
					# print 'redudancy'
					continue
				# ignore the visited url
				if link in self.done_url:
					continue
				if self.p_target == 0 and (self._link_type(link) == 'img' or self._link_type(link) == 'page'):
					# log.context(link)
					tmp_queue.put({"path":link})
				elif self.p_target == 1 and self._link_type(link) == 'page':
					tmp_queue.put({"path":link})
				elif self.p_target == 2:
					tmp_queue.put({"path":link})
			# BFS
			if self.p_algo == 0:
				for element in  list(tmp_queue.queue):
					# print element
					self.visit_queue.put(element)
			# DFS
			elif self.p_algo == 1:
				for element in list(self.visit_queue.queue):
					tmp_queue.put(element)
				self.visit_queue = tmp_queue 


			log.context('now queue %s' % str(list(self.visit_queue.queue)))


	def _ip_obfuscator(self,request,ip):
		'''
		to change the src ip address of the crawler, two approaches are used:
		1. for local, we add some special data to the GET parameter `fakeip` to illuminate the address
		2. for remote, we use some commecial proxy software to do so
		'''
		if not ip:
			random_ip = '.'.join([str(random.randint(1,254))  for x in range(4)])
		else:
			random_ip = ip

		query_dict =  dict(urlparse.parse_qsl(urlparse.urlparse(request['path']).query))
		query_dict['fakeip'] = random_ip
		new_query = '&'.join([k+'='+str(query_dict[k]) for k in query_dict])
		new_path = urlparse.urlparse(request['path']).path  + '?' + new_query
		request['path'] = new_path

		return request
		

	def _label_session(self,request):

		query_dict =  dict(urlparse.parse_qsl(urlparse.urlparse(request['path']).query))
		query_dict['real_session'] = self.real_session
		new_query = '&'.join([k+'='+str(query_dict[k]) for k in query_dict])
		new_path = urlparse.urlparse(request['path']).path  + '?' + new_query
		request['path'] = new_path

		return request

	def _patch_alert(self,driver):
		log.warning('patching alert...')
		if 'firefox' in driver.__str__():
			if hasattr(driver.switch_to,'alert'):
				# driver.switch_to.alert.accept()
				Alert(driver).dismiss()

		elif 'chrome' in driver.__str__():
			if hasattr(driver.switch_to,'alert'):
				driver.switch_to.alert.accept()
				Alert(driver).dismiss()


		# if 'firefox' in driver.__str__() and if hasattr(driver.switch_to,'alert'):
		# 	Alert(driver).accept()
		# elif 'chrome' in driver.__str__():
		# 	if hasattr(driver.switch_to,'alert'):
		# 		driver.switch_to.alert.accept()
		# 		Alert(driver).dismiss()
			


	def _send_request(self,driver,request):

		try:
			log.info('sending request to %s' %request['path'])
			driver.get(self.base_url + request['path'])
			self._patch_alert(driver)
			return {"content":driver.page_source}
		except Exception as e:
			log.error('error: ' + str(e)) 

	def run(self):
		'''
		run the crawler
		'''

		r = robot()
		d = r.random_driver(self.p_driver)
		start_time = time.time()
		self._init_visit_queue()
		session_span = 600
		request_count = 0
		request_paths = []
		static_ip = '.'.join([str(random.randint(1,254))  for x in range(4)])

		while (time.time() - start_time < session_span) and not self.visit_queue.empty():

			request = self.visit_queue.get()
			if request['path'].split('?')[0] in self.done_url:
				continue 
			self.done_url.append(request['path'].split('?')[0])
			request = self._label_session(request)
			if self.p_adv == 1:
				request = self._ip_obfuscator(request,'')
			elif self.p_adv == 2:
				request = self._ip_obfuscator(request,'10.10.10.10')

			response = self._send_request(d,request)
			self.crawling_algorithm(response,request)
			self.crawling_sleep()
			request_count += 1
			request_paths.append(request['path'])
			log.info('now queue %d' % len(list(self.visit_queue.queue)))
			log.info('total request number: %d' %request_count)
		# print open('visit.log', 'w').write('\n'.join(request_paths))



if __name__ == '__main__':

	# c = crawler('http://172.25.102.82:8002',0,[0,1,0,0,0])
	# c.run()


	thread_array = []
	thread_num = 5

	driver_array = range(0,22)
	random.shuffle(driver_array)
	# print(driver_array[:5])
	for i in range(thread_num):

		# policy array interpretation
		# policy_array = [target, speed, algo, driver, adv]

		

		policy_array = [random.randint(0,4),random.randint(0,4),random.randint(0,1),driver_array[i],random.randint(0,1)]
		# policy_array = [0,3,random.randint(0,1), 10,random.randint(0,1)]

		print 'policy_array: %s' %str(policy_array)
		c = crawler('http://172.16.10.1:8001',0,policy_array)
		t = threading.Thread(target=c.run,name='crawler_thread_%d'%(i+1))	
		log.success('one thread for crawling has been created!')
		thread_array.append(t)


	for t in thread_array:
		t.setDaemon(True)
		t.start()

	try:
		while 1:
			time.sleep(5)
	except KeyboardInterrupt:
		log.error("Program stoped by user, existing...")
		sys.exit()



		



