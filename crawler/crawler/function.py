#!/usr/bin/env python
from BeautifulSoup import BeautifulSoup

def get_all_links(html,prefix,url):
	# get all the links accociated with special prefix

	links = []
	final_links = []

	soup = BeautifulSoup(html)

	# normal link
	for a in soup.findAll('a', href=True):
		# print "Found the a URL:", a['href']
		links.append(a['href'])

	# css
	for link in soup.findAll('link', href=True):
		# print "Found the css URL:", link['href']
		links.append(link['href'])

	# javascript
	for js in soup.findAll('script',src=True):
		# print "Found the js URL:", js['src']
		links.append(js['src'])

	# img
	for img in soup.findAll('img',src=True):
		# print "Found the img URL:", img['src']
		links.append(img['src'])

	# link filter
	for link in links:

		if not link:
			pass
		elif link == '#':
			# ignore the #
			pass
		elif link.startswith('data:'):
			# ignore the raw image data
			pass
		elif link.startswith('mailto:'):
			# ignore the mail protocol
			pass
		elif (link.startswith('https://') or link.startswith('http://') or link.startswith('//')) and not link.startswith(prefix):
			# not the internal links
			pass
		elif link.startswith(prefix):
			final_links.append(link.replace(prefix,'').replace('//','/'))
		elif link.startswith('javascript'):
			# skip javascript href
			pass
		elif link.startswith('/'):
			# absolute path
			final_links.append( link)
		elif link.startswith('../'):
			print 'hello ' + link
			tmp_url = '/'.join(url.split('/')[:-1]) + '/' +  '/'.join(link.split('/')[2:])
			tmp_url = tmp_url.replace('//','/')
			final_links.append(tmp_url)
		else:
			# relative path
			if '/' not in link and '?' not in link:
				tmp_url = '/'.join(url.split('/')[:-1]) + '/' + link
			else:
				tmp_url = '/'.join(url.split('/')[:-1]) + '/' +  '/'.join(link.split('/')[1:])
			print 'hi ' + tmp_url

			tmp_url = tmp_url.replace('//','/')
			final_links.append(tmp_url)

		final_links = list(set(final_links))

	return final_links