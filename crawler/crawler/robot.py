#!/usr/bin/env python
# -*- coding: utf-8 -*-

from selenium import webdriver
from BeautifulSoup import BeautifulSoup
from selenium.common.exceptions import UnexpectedAlertPresentException
from selenium.webdriver.common.alert import Alert
import sys
import os
import random
reload(sys)  
sys.setdefaultencoding('utf8')


class robot():
	def __init__(self):
		self.webdriver = ''

	def get_fp(self,browser):
		fp_url = 'http://172.16.10.1:8002/test/'
		browser.get(fp_url)
		soup = BeautifulSoup(browser.page_source)

		for a in soup.findAll('h1'):
			print a.contents[0]

	def get_dynamic(self,browser):
		url = 'http://172.16.10.1:8001/'
		try:

			browser.get(url)
			# fix the bug by alert
			if 'firefox' in browser.__str__():
				Alert(browser).accept()
			elif 'chrome' in browser.__str__():
				browser.switch_to.alert.accept()
				Alert(browser).dismiss()

			tmp = browser.page_source
			print 'done'
		except UnexpectedAlertPresentException,e:
			print 'error: ' + str(e)
			browser.close()

	def portscan(self,browser):
		url = 'http://47.91.145.204/portscan.html'
		try:
			browser.get(url)
			print browser.page_source
			print 'done'
		except UnexpectedAlertPresentException,e:
			print 'error: ' + str(e)

	def _init_chrome(self,binary='/root/other/chrome/579575/chrome-linux/chrome'):
		self.webdriver = 'chrome'
		options = webdriver.ChromeOptions()
		options.binary_location = binary
		options.add_argument('headless')
		options.add_argument('no-sandbox')
		options.add_argument('window-size=1200x600')
		browser = webdriver.Chrome(executable_path='/root/chromedriver',chrome_options=options)
		
		return browser

	def _init_firefox(self,binary='/root/other/firefox/firefox-59.0/firefox/firefox-bin'):
		self.webdriver = 'firefox'
		options = webdriver.FirefoxOptions()
		options.binary_location =  binary
		options.add_argument("--headless")
		browser = webdriver.Firefox(executable_path='/root/firefox/geckodriver',firefox_options=options)

		return browser



	def _init_phantomjs(self,binary='/root/other/phantomjs/phantomjs-2.1.1-linux-x86_64/bin/phantomjs'):
		self.webdriver = 'phantomjs'
		phantomjs_path = binary
		browser = webdriver.PhantomJS(executable_path = phantomjs_path)

		return browser

	def random_driver(self,rand=random.randint(0,22)):
		chrome_ops = [579575,579948,580496,580769,581137,581504,581780,582152]
		phantomjs_ops = ['1.9.7','1.9.8','2.1.1']
		firefox_ops = range(57,69)

		ops_len = len(chrome_ops) + len(phantomjs_ops) + len(firefox_ops)
		# rand = 3
		if rand < len(chrome_ops):
			op = chrome_ops[rand]
			binary_location = '/root/other/chrome/%d/chrome-linux/chrome' %op
			return self._init_chrome(binary_location)
		rand -= len(chrome_ops)

		if rand < len(phantomjs_ops):
			op = phantomjs_ops[rand]
			binary_location = '/root/other/phantomjs/phantomjs-%s-linux-x86_64/bin/phantomjs' %op
			return self._init_phantomjs(binary_location)
		rand -= len(phantomjs_ops)

		if rand < len(firefox_ops):
			op = firefox_ops[rand]
			binary_location = '/root/other/firefox/firefox-%d.0/firefox/firefox-bin' %op
			return self._init_firefox(binary_location)

		return False


	def visit(self,browser,url):
		browser.get(url)
		return browser.page_source


	def __del__(self):
		# kill all the process
		# os.system('killall firefox-bin chrome chromedriver phantomjs geckodriver 2>/dev/null')
		pass


if __name__ == '__main__':
	# r = robot()
	'''	
	chrome_dirs = [579575,579948,580496,580769,581137,581504,581780,582152]

	for i in chrome_dirs:
		binary_location = '/root/other/chrome/%d/chrome-linux/chrome' %i
		r.get_fp(r._init_chrome(binary_location))

	for i in ['1.9.7','1.9.8','2.1.1']:
		binary_location = '/root/other/phantomjs/phantomjs-%s-linux-x86_64/bin/phantomjs' %i
		r.get_fp(r._init_phantomjs(binary_location))

	for i in range(57,69):
		binary_location = '/root/other/firefox/firefox-%d.0/firefox/firefox-bin' %i
		r.get_fp(r._init_firefox(binary_location))
	'''
	r = robot()
	d = r.random_driver()
	print d
	print r.get_dynamic(d)
	d.close()


