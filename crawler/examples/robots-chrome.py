#!/usr/bin/env python
# -*- coding: utf-8 -*-
from selenium import webdriver
from BeautifulSoup import BeautifulSoup
import sys
reload(sys)  
sys.setdefaultencoding('utf8')

'''
snippet for Chinese character support
'''

# options = webdriver.ChromeOptions()
# options.binary_location = '/root/test/chrome-linux/chrome'
# options.add_argument('headless')
# options.add_argument('no-sandbox')
# options.add_argument('window-size=1200x600')

# browser = webdriver.Chrome(executable_path='/root/chromedriver',chrome_options=options)
# browser.get('http://172.16.10.1:8001/')
# browser.get_screenshot_as_file('main-page.png')

def get_all_links(html,prefix,url):
	# get all the links accociated with special prefix

	links = []
	final_links = []

	soup = BeautifulSoup(html)

	# normal link
	for a in soup.findAll('a', href=True):
		# print "Found the a URL:", a['href']
		links.append(a['href'])

	# css
	for link in soup.findAll('link', href=True):
		# print "Found the css URL:", link['href']
		links.append(link['href'])

	# javascript
	for js in soup.findAll('script',src=True):
		# print "Found the js URL:", js['src']
		links.append(js['src'])

	# img
	for img in soup.findAll('img',src=True):
		# print "Found the img URL:", img['src']
		links.append(img['src'])

	# link filter
	for link in links:

		if not link:
			pass
		elif link == '#':
			# ignore the #
			pass
		elif link.startswith('data:'):
			# ignore the raw image data
			pass
		elif link.startswith('mailto:'):
			# ignore the mail protocol
			pass
		elif (link.startswith('https://') or link.startswith('http://') or link.startswith('//')) and not link.startswith(prefix):
			# not the internal links
			pass
		elif link.startswith(prefix):
			final_links.append(link)
		elif link.startswith('javascript'):
			# skip javascript href
			pass
		elif link.startswith('/'):
			# absolute path
			final_links.append(prefix + link)
		else:
			# relative path
			final_links.append(prefix + url + link)

		final_links = list(set(final_links))

	return final_links






options = webdriver.ChromeOptions()
options.binary_location = '/root/other/chrome/579575/chrome-linux/chrome'
options.add_argument('headless')
options.add_argument('no-sandbox')
options.add_argument('window-size=1200x600')

browser = webdriver.Chrome(executable_path='/root/chromedriver',chrome_options=options)
browser.get('http://172.16.10.1:8002/')


print get_all_links(browser.page_source,'http://172.16.10.1:8002','/')
browser.close()
