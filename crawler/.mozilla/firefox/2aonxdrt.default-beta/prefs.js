// Mozilla User Preferences

// DO NOT EDIT THIS FILE.
//
// If you make changes to this file while the application is running,
// the changes will be overwritten when the application exits.
//
// To change a preference value, you can either:
// - modify it via the UI (e.g. via about:config in the browser); or
// - set it within a user.js file in your profile.

user_pref("app.normandy.first_run", false);
user_pref("app.normandy.user_id", "b0036769-5856-4726-9574-65c886fb4848");
user_pref("app.update.lastUpdateTime.browser-cleanup-thumbnails", 0);
user_pref("app.update.lastUpdateTime.recipe-client-addon-run", 0);
user_pref("app.update.lastUpdateTime.xpi-signature-verification", 0);
user_pref("browser.bookmarks.restore_default_bookmarks", false);
user_pref("browser.cache.disk.amount_written", 66);
user_pref("browser.cache.disk.filesystem_reported", 1);
user_pref("browser.contentblocking.category", "standard");
user_pref("browser.laterrun.bookkeeping.profileCreationTime", 1562671157);
user_pref("browser.laterrun.bookkeeping.sessionCount", 2);
user_pref("browser.laterrun.enabled", true);
user_pref("browser.migration.version", 81);
user_pref("browser.newtabpage.activity-stream.impressionId", "{28aeef58-3123-4b36-9363-eb12ce5dd3c8}");
user_pref("browser.newtabpage.storageVersion", 1);
user_pref("browser.pageActions.persistedActions", "{\"version\":1,\"ids\":[\"bookmark\",\"pinTab\",\"bookmarkSeparator\",\"copyURL\",\"emailLink\",\"addSearchEngine\",\"sendToDevice\",\"pocket\",\"webcompat-reporter_mozilla_org\",\"screenshots_mozilla_org\"],\"idsInUrlbar\":[\"pocket\",\"bookmark\"]}");
user_pref("browser.pagethumbnails.storage_version", 3);
user_pref("browser.safebrowsing.provider.mozilla.lastupdatetime", "1562671171535");
user_pref("browser.safebrowsing.provider.mozilla.nextupdatetime", "1562674771535");
user_pref("browser.search.region", "SG");
user_pref("browser.shell.didSkipDefaultBrowserCheckOnFirstRun", true);
user_pref("browser.slowStartup.averageTime", 2117);
user_pref("browser.slowStartup.samples", 1);
user_pref("browser.startup.homepage_override.buildID", "20190603181408");
user_pref("browser.startup.homepage_override.mstone", "68.0");
user_pref("browser.startup.lastColdStartupCheck", 1562671164);
user_pref("browser.uiCustomization.state", "{\"placements\":{\"widget-overflow-fixed-list\":[],\"nav-bar\":[\"back-button\",\"forward-button\",\"stop-reload-button\",\"home-button\",\"customizableui-special-spring1\",\"urlbar-container\",\"customizableui-special-spring2\",\"downloads-button\",\"library-button\",\"sidebar-button\",\"fxa-toolbar-menu-button\"],\"toolbar-menubar\":[\"menubar-items\"],\"TabsToolbar\":[\"tabbrowser-tabs\",\"new-tab-button\",\"alltabs-button\"],\"PersonalToolbar\":[\"personal-bookmarks\"]},\"seen\":[\"developer-button\"],\"dirtyAreaCache\":[\"nav-bar\"],\"currentVersion\":16,\"newElementCount\":2}");
user_pref("browser.urlbar.placeholderName", "Google");
user_pref("datareporting.policy.dataSubmissionPolicyAcceptedVersion", 2);
user_pref("datareporting.policy.dataSubmissionPolicyNotifiedTime", "1562671168248");
user_pref("devtools.onboarding.telemetry.logged", true);
user_pref("distribution.iniFile.exists.appversion", "68.0");
user_pref("distribution.iniFile.exists.value", false);
user_pref("dom.push.userAgentID", "170904e88387477d8d9ef78ea57bb6f9");
user_pref("extensions.activeThemeID", "default-theme@mozilla.org");
user_pref("extensions.blocklist.pingCountVersion", 0);
user_pref("extensions.databaseSchema", 31);
user_pref("extensions.getAddons.databaseSchema", 5);
user_pref("extensions.incognito.migrated", true);
user_pref("extensions.lastAppBuildId", "20190603181408");
user_pref("extensions.lastAppVersion", "68.0");
user_pref("extensions.lastPlatformVersion", "68.0");
user_pref("extensions.pendingOperations", false);
user_pref("extensions.systemAddonSet", "{\"schema\":1,\"addons\":{}}");
user_pref("extensions.webcompat.perform_injections", true);
user_pref("extensions.webcompat.perform_ua_overrides", true);
user_pref("extensions.webextensions.uuids", "{\"formautofill@mozilla.org\":\"aadae1fc-fb48-4e15-b637-94010f61d1b5\",\"fxmonitor@mozilla.org\":\"22bf9f86-f17f-4d7d-ad73-e5abd3c87562\",\"screenshots@mozilla.org\":\"e839c7a5-2e8d-44d2-bd21-07d143dda321\",\"webcompat-reporter@mozilla.org\":\"633bc49a-8406-4bbd-80fe-ff6e26860686\",\"webcompat@mozilla.org\":\"c5e35312-abd2-4a63-a2e4-17e1e6d97ca6\",\"default-theme@mozilla.org\":\"870c0439-2640-4199-82a1-227dbe8b0963\",\"google@search.mozilla.org\":\"adda48d4-1bd3-4c0b-a3a6-3aa29285adfd\",\"amazondotcom@search.mozilla.org\":\"4deaf8c8-ea93-4acb-94d0-dea498468103\",\"bing@search.mozilla.org\":\"aa5bc98c-2f08-47a0-bca0-17691b429355\",\"ddg@search.mozilla.org\":\"98ecbca5-0fab-44c7-96a4-e51e9853f9b4\",\"ebay@search.mozilla.org\":\"6b16a141-53e8-477b-9704-1c92f5d2e060\",\"twitter@search.mozilla.org\":\"6d4eaf2c-e73f-49b3-b439-42843bf6d281\",\"wikipedia@search.mozilla.org\":\"f75bdbf3-0304-4413-9bf4-5f78347cfb44\"}");
user_pref("gfx.blacklist.canvas2d.acceleration", 4);
user_pref("gfx.blacklist.canvas2d.acceleration.failureid", "FEATURE_FAILURE_GLXTEST_FAILED");
user_pref("gfx.blacklist.d3d11.keyed.mutex", 4);
user_pref("gfx.blacklist.d3d11.keyed.mutex.failureid", "FEATURE_FAILURE_GLXTEST_FAILED");
user_pref("gfx.blacklist.direct2d", 4);
user_pref("gfx.blacklist.direct2d.failureid", "FEATURE_FAILURE_GLXTEST_FAILED");
user_pref("gfx.blacklist.direct3d11angle", 4);
user_pref("gfx.blacklist.direct3d11angle.failureid", "FEATURE_FAILURE_GLXTEST_FAILED");
user_pref("gfx.blacklist.dx.interop2", 4);
user_pref("gfx.blacklist.dx.interop2.failureid", "FEATURE_FAILURE_GLXTEST_FAILED");
user_pref("gfx.blacklist.dx.nv12", 4);
user_pref("gfx.blacklist.dx.nv12.failureid", "FEATURE_FAILURE_GLXTEST_FAILED");
user_pref("gfx.blacklist.dx.p010", 4);
user_pref("gfx.blacklist.dx.p010.failureid", "FEATURE_FAILURE_GLXTEST_FAILED");
user_pref("gfx.blacklist.dx.p016", 4);
user_pref("gfx.blacklist.dx.p016.failureid", "FEATURE_FAILURE_GLXTEST_FAILED");
user_pref("gfx.blacklist.gpu.process", 4);
user_pref("gfx.blacklist.gpu.process.failureid", "FEATURE_FAILURE_GLXTEST_FAILED");
user_pref("gfx.blacklist.hardwarevideodecoding", 4);
user_pref("gfx.blacklist.hardwarevideodecoding.failureid", "FEATURE_FAILURE_GLXTEST_FAILED");
user_pref("gfx.blacklist.layers.advanced", 4);
user_pref("gfx.blacklist.layers.advanced.failureid", "FEATURE_FAILURE_GLXTEST_FAILED");
user_pref("gfx.blacklist.layers.direct3d10", 4);
user_pref("gfx.blacklist.layers.direct3d10-1", 4);
user_pref("gfx.blacklist.layers.direct3d10-1.failureid", "FEATURE_FAILURE_GLXTEST_FAILED");
user_pref("gfx.blacklist.layers.direct3d10.failureid", "FEATURE_FAILURE_GLXTEST_FAILED");
user_pref("gfx.blacklist.layers.direct3d11", 4);
user_pref("gfx.blacklist.layers.direct3d11.failureid", "FEATURE_FAILURE_GLXTEST_FAILED");
user_pref("gfx.blacklist.layers.direct3d9", 4);
user_pref("gfx.blacklist.layers.direct3d9.failureid", "FEATURE_FAILURE_GLXTEST_FAILED");
user_pref("gfx.blacklist.layers.opengl", 4);
user_pref("gfx.blacklist.layers.opengl.failureid", "FEATURE_FAILURE_GLXTEST_FAILED");
user_pref("gfx.blacklist.stagefright", 4);
user_pref("gfx.blacklist.stagefright.failureid", "FEATURE_FAILURE_GLXTEST_FAILED");
user_pref("gfx.blacklist.webgl.angle", 4);
user_pref("gfx.blacklist.webgl.angle.failureid", "FEATURE_FAILURE_GLXTEST_FAILED");
user_pref("gfx.blacklist.webgl.msaa", 4);
user_pref("gfx.blacklist.webgl.msaa.failureid", "FEATURE_FAILURE_GLXTEST_FAILED");
user_pref("gfx.blacklist.webgl.opengl", 4);
user_pref("gfx.blacklist.webgl.opengl.failureid", "FEATURE_FAILURE_GLXTEST_FAILED");
user_pref("gfx.blacklist.webgl2", 4);
user_pref("gfx.blacklist.webgl2.failureid", "FEATURE_FAILURE_GLXTEST_FAILED");
user_pref("gfx.blacklist.webrender", 4);
user_pref("gfx.blacklist.webrender.failureid", "FEATURE_FAILURE_GLXTEST_FAILED");
user_pref("gfx.blacklist.webrtc.hw.acceleration", 4);
user_pref("gfx.blacklist.webrtc.hw.acceleration.decode", 4);
user_pref("gfx.blacklist.webrtc.hw.acceleration.decode.failureid", "FEATURE_FAILURE_GLXTEST_FAILED");
user_pref("gfx.blacklist.webrtc.hw.acceleration.encode", 4);
user_pref("gfx.blacklist.webrtc.hw.acceleration.encode.failureid", "FEATURE_FAILURE_GLXTEST_FAILED");
user_pref("gfx.blacklist.webrtc.hw.acceleration.failureid", "FEATURE_FAILURE_GLXTEST_FAILED");
user_pref("media.gmp.storage.version.observed", 1);
user_pref("pdfjs.enabledCache.state", true);
user_pref("pdfjs.migrationVersion", 2);
user_pref("pdfjs.previousHandler.alwaysAskBeforeHandling", true);
user_pref("places.history.expiration.transient_current_max_pages", 112348);
user_pref("plugin.disable_full_page_plugin_for_types", "application/pdf");
user_pref("privacy.sanitize.pending", "[{\"id\":\"newtab-container\",\"itemsToClear\":[],\"options\":{}}]");
user_pref("security.sandbox.content.tempDirSuffix", "9a042a74-602e-456e-87d7-44267e464154");
user_pref("security.sandbox.plugin.tempDirSuffix", "84ec3c7e-47a3-45e7-844a-94a97e79883f");
user_pref("signon.importedFromSqlite", true);
user_pref("toolkit.startup.last_success", 1561700251);
user_pref("toolkit.startup.recent_crashes", 2);
user_pref("toolkit.telemetry.cachedClientID", "c151a584-2824-455c-bb04-383df84f489b");
user_pref("toolkit.telemetry.previousBuildID", "20190603181408");
user_pref("toolkit.telemetry.reportingpolicy.firstRun", false);
