"use strict";
var page = require('webpage').create(),address, output,system = require('system');

address = system.args[1];
output = "out.png";

page.open(address, function (status) {
    if (status !== 'success') {
        console.log('Unable to load the address!');
        phantom.exit(1);
    } else {
        window.setTimeout(function () {
            page.render(output);
            phantom.exit();
        }, 200);
    }
});
